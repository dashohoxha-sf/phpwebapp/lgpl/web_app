<?


/**************************************************************
/*			class RepeatTpl
/* A template that is repeated for each record of a recordset.
/*************************************************************/

class RepeatTpl extends Template
{
	var $rsId;		//the id of the recordset
	var $if_empty;	//the template that is displayed if the RS is empty
	var $header;	//the header template
	var $footer;	//the footer template
	var $separator;	//the separator template
	
	function RepeatTpl($rs_id)
	{
		static $id = "Repeat_00";
		Template::Template($id++, "RepeatTpl");	//call the constructor of the superclass	
		
		$this->rsId = $rs_id;
		$this->if_empty = new Template($this->id."_IfEmpty", "IfEmpty");
		$this->header = new Template($this->id."_Header", "Header");
		$this->footer = new Template($this->id."_Footer", "Footer");
		$this->separator = new Template($this->id."_Separator", "Separator");
	}
	
	function toHtml()
	//overrides the function of the superclass
	{
		global $webPage, $tplVars, $session;
		
		$rs = $webPage->getRecordset($this->rsId);
		if ($rs=="undefined")
		{
			$err_msg = WebApp::error_msg("Recordset '$this->rsId' is undefined.");
			$webPage->print_line($err_msg);
			Template::toHtml();
			return;
		}
		
		$tplVars->enterNewScope();	//a new scope for the Repeat element

		//open the recordset
		if ( $rs->isPageRS() )
		{
			$cp = $session->Vars[$rs->ID."_CP"];	//get current page
			$rs->Open($cp);	
			//add page vars (PrevPage, NextPage, etc)
			$rs_page_vars = $rs->getPageVars();
			$tplVars->addVars($rs_page_vars);
		}
		else
		{
			$rs->Open();
		}
		
		if ($rs->EOF())	//recordset has no records
		{
			$this->if_empty->toHtml();
		}
		else
		{
			//render the header
			$this->header->toHtml();

			//repeat the body for each record
			$row_nr = 1;
			while ( !$rs->EOF() )
			{
				$tplVars->enterNewScope();

				//add variables {{CurrentRecNr}} and {{CurrentRowNr}}
				$firstRecNr = $rs->isPageRS() ? WebApp::getVar("FirstRec") : 1;
				WebApp::addVar("CurrentRecNr", $firstRecNr + $row_nr - 1);
				WebApp::addVar("CurrentRowNr", $row_nr);

				//render the body of the repeat
				$tplVars->addVars($rs->Fields());
				$tplVars->addVars($this->getVars());
				Template::toHtml();

				if ($row_nr < $rs->count) 
				{
					//render the separator
					$this->separator->toHtml();
				}

				$tplVars->leaveScope();
				$rs->MoveNext();
				$row_nr++;
			}

			//render the footer
			$this->footer->toHtml();
		}
		
		$tplVars->leaveScope();
	}
	
	function to_text()
	//print the data of the template	(for debug)
	{
		global $webPage;
		
		$rs = $webPage->getRecordset($this->rsId);
		print "query: $rs->query\n";
		Template::to_text();
		print "if_empty : \n";
		$this->if_empty->to_text();
		print "header : \n";
		$this->header->to_text();
		print "footer : \n";
		$this->footer->to_text();
		print "separator : \n";
		$this->separator->to_text();
	}
}

?>