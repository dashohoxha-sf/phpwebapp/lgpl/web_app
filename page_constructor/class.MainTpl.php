<?


/**************************************************************
/*			class MainTpl
/* Represents the main (root) template in the page configuration.
/*************************************************************/

class MainTpl extends Template
{
	var $head;	//array of lines of the head of the page
	var $body;	//array of lines of the body of the page
	
	function MainTpl($filename)
	{
		//ereg("([^/]*)\.html?$", $filename, $regs);
		//$id = $regs[1];
		$id = $filename;
		//call the constructor of the superclass
		$this->Template($id, "MainTpl");	
		
		$this->filename = $filename;
		$this->head = array();
		$this->body = array();
		
		//parse the file and build the template tree
		$this->parse_file($filename);
		
		//append this file to each template
		$this->parse_file(WEBAPP_PATH."append.html");
	}	
	
	function to_text()
	//print the data of the template	(for debug)
	{
		print "head : \n";
		print "body : \n";
		Template::to_text();
	}
	
	function toHtml()
	{
		Template::toHtml();
	}

}

?>