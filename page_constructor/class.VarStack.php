<?
if ( !defined("class_VarStack") )
//test so that it is not included multiple times
{
define("class_VarStack", 1);

/**********************************************************************/
/*			CLASS VARSTACK
/* A stack of associative arrays, keeping variables in nesting scopes.
/**********************************************************************/

class VarStack
{
	var $Vars;

	function VarStack()
	{
		$this->Vars = array();
		$this->enterNewScope();
	}

	function enterNewScope()
	{
		array_push($this->Vars, array());
	}

	function leaveScope()
	{
		array_pop($this->Vars);
	}

	function addVar($var_name, $var_value)
	//add a variable to the current scope
	{
		$len = count($this->Vars);
		$current_scope = &$this->Vars[$len-1];
		$current_scope[$var_name] = $var_value;
	}
	
	function addGlobalVar($var_name, $var_value)
	//add a variable to the first scope
	{
		$first_scope = &$this->Vars[0];
		$first_scope[$var_name] = $var_value;
	}

	function addVars($arrVars)
	//add a list of vars to the current scope
	{
		$len = count($this->Vars);
		$current_scope = &$this->Vars[$len-1];
		$current_scope = array_merge($current_scope, $arrVars);
	}
	
	function addGlobalVars($arrVars)
	//add a list of vars to the first scope
	{
		$first_scope = &$this->Vars[0];
		$first_scope = array_merge($first_scope, $arrVars);
	}

	function getVar($var_name)
	//returns the value of a variable; if it is not found
	//in the current scope, the scope above it is searched,
	//and so on; if it is not found at all, "undefined" is returned
	{
		$found = false;
		$len = count($this->Vars);
		for ($i = $len-1; $i>=0; $i--)
		{
			$scope = &$this->Vars[$i];
			if (isset($scope[$var_name]))
			{
				$found = true;
				$var_value = &$scope[$var_name];
				break;
			}
		}
		if ($found)	return $var_value;
		else 		return "undefined";
	}

	////////////////////////////////////////////
	////////////////////////////////////////////
	function printOut()		//for debug
	{
		print "\n<xmp>\n";
		$len = count($this->Vars);
		for ($i = $len-1; $i>=0; $i--)
		{
			print "\n==== Scope $i =======\n";
			$scope = &$this->Vars[$i];
			while (list($var_name,$var_value) = each($scope))
			{
				print "$var_name='$var_value'  \n";
			}
			reset($scope);
		}
		print "\n</xmp>\n";
	}

	////////////////////////////////////////////
	function test()		//for debug
	{
		$vars = new VarStack;

		//$vars->enterNewScope();
		$vars->addVars(array("a"=>"XXX", "b"=>"BBB", "c"=>""));
		$vars->enterNewScope();
		$vars->addVar("a", "XXX");
		$vars->enterNewScope();
		$vars->addVars(array("a"=>"AAA", "b"=>"YYY"));
		$vars->enterNewScope();
		$vars->addGlobalVar("g1", "GGG");

		/*
		print " var a='".$vars->getVar("a")."' \n";
		print " var c='".$vars->getVar("c")."' \n";
		print " var x='".$vars->getVar("x")."' \n";
		$vars->printOut();
		return;
		*/

		$vars->leaveScope();
		$vars->leaveScope();

		/*
		print " var a='".$vars->getVar("a")."' \n";
		print " var c='".$vars->getVar("c")."' \n";
		print " var x='".$vars->getVar("x")."' \n";
		$vars->printOut();
		return;
		*/

		$vars->enterNewScope();
		$vars->addVars(array("a"=>"AAA", "b"=>"BBB"));
		$vars->enterNewScope();
		$vars->addGlobalVar("g2", "GGG2");
		$vars->addGlobalVar("g1", "GGG1");
		$vars->addVar("a", "XXX");
		$vars->enterNewScope();
		$vars->addVars(array("a"=>"AAA", "b"=>"YYY"));

		print " var g1='".$vars->getVar("g1")."' \n";
		print " var a='".$vars->getVar("a")."' \n";
		print " var c='".$vars->getVar("c")."' \n";
		print " var x='".$vars->getVar("x")."' \n";
		$vars->printOut();
		return;
	}

}	//End of class VarStack

}	// End if defined
?>