<?


/**************************************************************
/*			class WebBox
/* A template that has also its own client-side and server-side
/* logic and is almost a self-contained unit (so that it can be 
/* included easily into other web pages or other WebBox-es).
/*************************************************************/

class WebBox extends Template
{	
	function WebBox($id)
	{
		$this->Template($id, "WebBox");
	}
	
	function loadPhpCode()
	//This is called before the WebBox template is parsed.
	//It includes a PHP file that contains the server-side
	//logic of the WebBox. The name of this file is the same
	//as the id of the WebBox, with the extenssion ".php".
	{	
		//before loading the php code, load the .db file of the webbox
		$this->parse_db_file();
		
		//look for the file in the same folder as WebBox
		//and include it if it exists
		$path = dirname($this->filename);
		$fname = $path."/".$this->id.".php";
		if (file_exists($fname))	include_once $fname;
		
		//call the function eventHandler if it exists
		$eventHandler = $this->id."_eventHandler";
		if (function_exists($eventHandler))
		{
			global $event;
			if ( $event->target==$this->id 
				or $event->target=="any" )
			{
				$eventHandler($event);
			}
		}
		
		//call the function onLoad if it exists
		$onLoad = $this->id."_onLoad";
		if (function_exists($onLoad))		$onLoad();
	}
	
	function parse_db_file()
	//parse the .db file of the webbox, if it exists
	{
		$path = dirname($this->filename);
		$fname = $path."/".$this->id.".db";
		if (file_exists($fname))
		{
			//open and parse the file
			$fp = fopen($fname, "r");
			$this->parse($fp);
			fclose($fp);
		}
	}
		
	function toHtml()
	//overrides the function of the superclass
	{
		//call the function onRender if it exists
		$onRender = $this->id."_onRender";
		if (function_exists($onRender))		$onRender();
		
		Template::toHtml();
	}
	
	function to_text()
	//print the data of the template	(for debug)
	{
		Template::to_text();
	}
}

?>