<?


/**************************************************************
/*			class IfTpl
/* A template that is renderd if the condition evaluates to true.
/*************************************************************/

class IfTpl extends Template
{
	var $condition;

	function IfTpl($condition ="1")
	{
		static $id = "If_00";
		Template::Template($id++, "IfTpl");	//call the constructor of the superclass	
		$this->condition = $condition;
	}
	
	function toHtml()
	//overrides the function of the superclass
	//it renders the template only if the $condition 
	//evaluates to true
	{
		$expr = $this->condition;
		$expr = WebApp::replaceVars($expr);
		$val = WebApp::evaluate($expr);
		if ($val)	Template::toHtml();
	}
	
	function to_text()
	//print the data of the template	(for debug)
	{
		print "condition : '$this->condition'\n";
		Template::to_text();
	}
}

?>