<?

if ( !defined("class_WebPage") )
//test so that it is not included multiple times
{
define("class_WebPage", 1);

/***********************************************************
/*			Class WebPage
/*	Keeps the templates, recordsets etc. that are used
/*	to build an HTML page.
/***********************************************************/

class WebPage
{
	var $tpl_file;			//the file name of the main template
	var $rootTpl;			//reference to the main template of the page
	
	var $collect;			//if true, then the page is not outputed directly to the browser
	var $html_page;			//but is collected to the variable $html_page
	
	var $tpl_collection;	//collection of all the templates of the page
	var $rs_collection;		//collection of all the recordsets of the page


	function WebPage()
	{
		$this->collect = false;
		$this->html_page = "";
	}
	
	function print_line($line)
	//either sends the line to the browser or concats it
	//to the variable $this->html_page
	{
		if ($this->collect)
			$this->html_page .= $line;
		else
			print $line;
	}
	
	function load($tpl_file)
	//constructs the web page by loading the given template file
	{
		$this->tpl_file = $tpl_file;
		$this->tpl_collection = array();
		$this->rs_collection = array();
		
		$tpl = new MainTpl($this->tpl_file);
		$this->addTemplate($tpl);
		$this->rootTpl = &$tpl;
	}

	function addTemplate($tpl)
	//adds a template in the template collection
	{
		if (!isset($tpl->id))
		{
			print "Error:WebPage:addTemplate: unidentified template.\n";
			$tpl->toText();
			return;
		}
		$this->tpl_collection[$tpl->id] = $tpl;
	}

	function getTemplate($tpl_id)
	//Returns the template with the given id from the collection,
	//or "undefined" if a template with this id is not found.
	{
		if ( !isset($this->tpl_collection[$tpl_id]) )
		{
			return "undefined";
		}
		else
		{
			$tpl = &$this->tpl_collection[$tpl_id];
			return $tpl;
		}
	}
	
	function addRecordset($rs)
	//adds a recordset in the recordset collection
	{
		if (!isset($rs->ID) or $rs->ID=="undefined")
		{
			print "Error:WebPage:addRecordset: unidentified recordset.\n";
			$rs->toHtmlTable();
			return;
		}
		$this->rs_collection[$rs->ID] = $rs;
	}

	function getRecordset($rs_id)
	//Returns the recordset with the given id from the collection,
	//or "undefined" if a recordset with this id is not found.
	{
		if ( !isset($this->rs_collection[$rs_id]) )
		{
			return "undefined";
		}
		else
		{
			$rs = &$this->rs_collection[$rs_id];
			return $rs;
		}
	}

	function print_templates()		//for debugging load()
	//outputs the data of each template in $this->tpl_collection
	{
		reset($this->tpl_collection);
		while ( list($tplID,$tpl) = each($this->tpl_collection) )
		{
			$tpl->toText();
		}
	}
	
	function tpl_to_tree()	//for debugging load()
	//returns the structure of the loaded templates as a tree
	{
		$tree = "<hr>\n";
		$tree .= "<strong>The tree structure of the templates of the page: </strong>\n";
		$tree .= "\n<xmp>\n";
		$tree .= $this->rootTpl->to_tree("");
		$tree .= "</xmp>\n";
		$tree .= "<hr>\n";
		return $tree;
	}

	function recordsets_to_html()		//for debugging openRecordsets()
	//returns a string in html format which displays 
	//all the recordsets in $this->rs_collection
	{
		$html_recs = "<hr>\n";
		$html_recs .= "<b> THE RECORDSETS OF THE PAGE: </b>\n";
		reset($this->rs_collection);
		while ( list($rsId,$rs) = each($this->rs_collection) )
		{
			$html_recs .= $rs->toHtmlTable();
		}
		$html_recs .= "<hr>\n";
		return $html_recs;
	}

}  //  End of Class WebPage

}	// End if defined
?>