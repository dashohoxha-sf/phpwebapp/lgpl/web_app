<?


/**************************************************************
/*			class Template
/*************************************************************/

class Template
{
	var $id;		//template ID
	var $type;		//"Template", "If", "Repeat", "WebBox", "IfEmpty", "Header", "Footer"
	var $parent;	//the parent template
	
	var $vars;		//array that keeps the variables of the template
	var $content;	//the lines of the template (array of lines)
	var $filename;	//the file from which the template is loaded

	function Template($tplId ="undefined", $type ="Template")
	{
		static $default_id = "tpl_00";
		
		$this->id = ($tplId<>"undefined" ? $tplId : $default_id++);
		$this->type = $type;
		$this->vars = array();
		$this->content = array();
	}

	function getVars()
	//returns an array with all the evaluated variables
	{
		reset($this->vars);
		$tplVars = $this->vars;
		while ( list($var_name,$expr) = each($tplVars) )
		{
			$expr = WebApp::replaceVars($expr);
			$tplVars[$var_name] = WebApp::evaluate($expr);
		}
		return $tplVars;
	}

	function addVar($var_name, $expression)
	//adds a new variable in the array of variables
	{
		$var_name = $var_name;
		$this->vars[$var_name] = $expression;
	}
	
	
	/*****************************************************/
	/******* the functions that parse the templates *******/
	
	function parse_file($filename)
	//Opens the filename and loads the template from it.
	{		
		if ( !file_exists($filename) )
		{
			$this->content[] = WebApp::error_msg("file '$filename' not found");
			return;
		}
		
		global $tplVars;
		$tplVars->enterNewScope();
		
		//add the variable {{./}} which contains the current path
		$tpl_path = dirname($filename) . "/";
		$tplVars->addVar("./", $tpl_path);
		
		//open and parse the file
		$fp = fopen($filename, "r");
		$this->parse($fp);
		fclose($fp);

		$tplVars->leaveScope();
	}

	function parse($fp)
	//Loads the template from the given file pointer.
	//The loaded template is saved in the array $this->content.
	{
		global $webPage;

		while (!feof($fp))
		{
			$line = fgets($fp,896);
			switch ( WebApp::get_tag_name($line) )
			{
				case "<If":
					$this->parse_If($line, $fp);
					break;				
				case "<Repeat":
					$this->parse_Repeat($line, $fp);
					break;	
					
				case "<RepeatBody":
				case "</RepeatBody":
					//these are optional tags inside the <Repeat> tag
					//and are just ignored
					break;
					
				case "<IfEmpty":
					$this->parse_IfEmpty($line, $fp);
					break;		
				case "<Header":
					$this->parse_Header($line, $fp);
					break;	
				case "<Footer":
					$this->parse_Footer($line, $fp);
					break;	
				case "<Separator":
					$this->parse_Separator($line, $fp);
					break;	
				case "<WebBox":
					$this->parse_WebBox($line, $fp);
					break;	
				case "<MyTPL":
					$this->parse_MyTPL($line, $fp);
					break;

				case "</If":
				case "</Repeat":
				case "</WebBox":
				case "</IfEmpty":
				case "</Header":
				case "</Footer":
				case "</Separator":
				case "</Repeat":
				case "</MyTPL":
					return;	//this template is parsed
					break;
					
				case "<Include":
					$this->parse_Include($line);
					break;
					
				case "<Recordset":
					$this->parse_Recordset($line, $fp);
					break;
					
				case "<dbCommand":
					$this->parse_dbCommand($line, $fp);
					break;
					
				case "<Var":
					$this->parse_Var($line);
					break;
					
				case "<!--#":
					$this->parse_Comment($line, $fp);
					break;

				default:
					$this->content[] = $line;
					break;
			}
		}
	}
	
	function parse_If($line, $fp)
	{
		global $webPage;
		
		$condition = WebApp::get_attr_value($line, "condition");
		//create a new template and load it from the same file
		$tpl = new IfTpl($condition);
		$tpl->parent = &$this;
		$tpl->filename = $this->filename;
		$tpl->parse($fp);
		//set a pointer from this template to the new template
		$this->content[] = "##\n";
		$this->content[] = $tpl->id."\n";
		$webPage->addTemplate($tpl);
	}
	
	function parse_Repeat($line, $fp)
	{
		global $webPage;
		
		$rs_id = WebApp::get_attr_value($line, "rs");
		//create a new template and load it from the same file
		$tpl = new RepeatTpl($rs_id);
		$tpl->parent = &$this;
		$tpl->filename = $this->filename;
		$tpl->parse($fp);
		//set a pointer from this template to the new template
		$this->content[] = "##\n";
		$this->content[] = $tpl->id."\n";
		$webPage->addTemplate($tpl);
	}
	
	function parse_IfEmpty($line, $fp)
	{
		global $webPage;
		
		$tpl = new Template($this->id."_IfEmpty", "IfEmpty");
		$tpl->parent = &$this;
		$tpl->filename = $this->filename;
		$tpl->parse($fp);
		if ($this->type=="RepeatTpl")
			$this->if_empty = $tpl;
		else
			$this->content[] = WebApp::error_msg("'IfEmpty' element can be used only inside a 'Repeat' element");
		$webPage->addTemplate($tpl);
	}
	
	function parse_Header($line, $fp)
	{
		global $webPage;
		
		$tpl = new Template($this->id."_Header", "Header");
		$tpl->parent = &$this;
		$tpl->filename = $this->filename;
		$tpl->parse($fp);
		if ($this->type=="RepeatTpl")
			$this->header = $tpl;
		else
			$this->content[] = WebApp::error_msg("'Header' element can be used only inside a 'Repeat' element");
		$webPage->addTemplate($tpl);
	}
	
	function parse_Footer($line, $fp)
	{
		global $webPage;
		
		$tpl = new Template($this->id."_Footer", "Footer");
		$tpl->parent = &$this;
		$tpl->filename = $this->filename;
		$tpl->parse($fp);
		if ($this->type=="RepeatTpl")
			$this->footer = $tpl;
		else
			$this->content[] = WebApp::error_msg("'Footer' element can be used only inside a 'Repeat' element");
		$webPage->addTemplate($tpl);
	}
	
	function parse_Separator($line, $fp)
	{
		global $webPage;
		
		$tpl = new Template($this->id."_Separator", "Separator");
		$tpl->parent = &$this;
		$tpl->filename = $this->filename;
		$tpl->parse($fp);
		if ($this->type=="RepeatTpl")
			$this->separator = $tpl;
		else
			$this->content[] = WebApp::error_msg("'Separator' element can be used only inside a 'Repeat' element");
		$webPage->addTemplate($tpl);
	}
	
	function parse_WebBox($line, $fp)
	{
		global $webPage, $tplVars;
		
		//create a new template
		$id = WebApp::get_attr_value($line, "ID");
		$tpl = new WebBox($id);
		$tpl->parent = &$this;
		$tpl->filename = $this->filename;
		
		//load the PHP code of the WebBox,
		//execute the onLoad() function, and then parse it
		$tplVars->enterNewScope();
		$tpl->loadPhpCode();
		$tpl->parse($fp);
		$tplVars->leaveScope();
		
		//set a pointer from this template to the new template
		$this->content[] = "##\n";
		$this->content[] = $tpl->id."\n";
		$webPage->addTemplate($tpl);
	}
	
	function parse_MyTPL($line, $fp)
	{
		global $webPage;
		
		$id = WebApp::get_attr_value($line, "ID");
		//create a new template and load it from the same file
		$tpl = new Template($id, "MyTPL");
		$tpl->parent = &$this;
		$tpl->filename = $this->filename;
		$tpl->parse($fp);
		//set a pointer from this template to the new template
		$this->content[] = "##\n";
		$this->content[] = $tpl->id."\n";
		$webPage->addTemplate($tpl);
	}
	
	function parse_Include($line)
	{
		global $webPage;
		
		$tpl_filename = WebApp::get_attr_value($line, "SRC");			
		$tpl_filename = WebApp::replaceVars($tpl_filename);
		
		//create a new template and load it from $tpl_filename
		$tpl = new Template;
		$tpl->type = "Include";
		$tpl->parent = &$this;
		$tpl->filename = $tpl_filename;
		$tpl->parse_file($tpl_filename);
		//set a pointer from this template to the new template
		$this->content[] = "##\n";
		$this->content[] = $tpl->id."\n";
		$webPage->addTemplate($tpl);
	}
	
	function parse_Recordset($line, $fp)
	{
		global $webPage, $session;
		
		//get the attributes 'ID' and 'recs_per_page'
		$id = WebApp::get_attr_value($line, "ID");
		$rp = WebApp::get_attr_value($line, "recs_per_page");
		
		//get the query (between <Query> and </Query>)
		$line = fgets($fp,896);	
		if ( ereg("<Query>(.*)</Query>", $line, $regs) )
		{	
			//all the query is in one line
			$query = trim($regs[1]);
		}
		else	//the query is in several lines
		{
			$line = trim(fgets($fp,896));	
			while ($line<>"</Query>")
			{
				$query .= " ".$line;
				$line = trim(fgets($fp,896));	
			}
		}
		//consume the closing tag "</Recordset>
		$line = fgets($fp,896);
		
		//create the Recordset object
		if ($rp=="undefined")
		{
			$rs = new Recordset($id, $query);
		}
		else
		{
			$rs = new PageRecordset($id, $query, $rp);
			if (!isset($session->Vars[$id."_CP"]))
				$session->Vars[$id."_CP"] = 1;
		}
		
		$webPage->addRecordset($rs);
	}
	
	function parse_dbCommand($line, $fp)
	{
		global $webPage, $session;
		
		//get the attributes 'ID' and 'recs_per_page'
		$id = WebApp::get_attr_value($line, "ID");
		
		//get the query (between <Query> and </Query>)
		$line = fgets($fp,896);	
		if ( ereg("<Query>(.*)</Query>", $line, $regs) )
		{	
			//all the query is in one line
			$query = trim($regs[1]);
		}
		else	//the query is in several lines
		{
			$line = trim(fgets($fp,896));	
			while ($line<>"</Query>")
			{
				$query .= " ".$line;
				$line = trim(fgets($fp,896));	
			}
		}
		//consume the closing tag "</dbCommand>
		$line = fgets($fp,896);
		
		//create the Recordset object
		$rs = new Recordset($id, $query);
		
		$webPage->addRecordset($rs);
	}	
	
	function parse_Var($line)
	{
		ereg('<Var name="([^"]*)">(.*)</Var>', $line, $regs);
		$var_name = $regs[1];
		$expression = $regs[2];
		$this->addVar($var_name, $expression);
	}
	
	function parse_Comment($line, $fp)
	{
		$end_of_comment = ereg("#-->", $line);
		while (!$end_of_comment)
		{
			$line = fgets($fp,896);
			$end_of_comment = ereg("#-->", $line);
		}
	}
	
	/************ debug functions *****************/
	function to_text()
	//print the data of the template	(for debug)
	{		
		//print $this->vars
		print "vars :";
		reset($this->vars);
		while ( list($vname,$value) = each($this->vars) )
		{
			print "  $vname='$value'";
		}
		print "\n";
		
		//print $this->content
		print "-------- begin content ---------\n";
		for ($i=0; $i < sizeof($this->content); $i++)
		{
			$line = $this->content[$i];
			print $line;
		}
		print "-------- end content ---------\n";
	}
	
	function toText()
	//prints the data of this template	(for debug)
	{
		print "\n<xmp>\n";
		print "======== Begin Template: $this->id ========\n";
		print "type='$this->type' \t parent='".$this->parent->id."'\n";
		print "filename='$this->filename'\n";
		$this->to_text();
		print "======== End Template: $this->id =========\n";
		print "</xmp>\n";
	}
	
	function to_tree($indent)
	//returns the structure of loaded templates as a tree
	{
		global $webPage;
		
		$tree = $indent."+--ID='".$this->id."'";
		$tree .= " type='".$this->type."'";
		$tree .= " filename='".$this->filename."'\n";
		$i = 0;
		while ($i < sizeof($this->content))
		{
			$line = $this->content[$i++];
			if (trim($line)=="##")
			{
				$line = $this->content[$i++];
				$tpl_id = trim($line);
				$tpl = $webPage->getTemplate($tpl_id);
				$tree .= $tpl->to_tree($indent."|  ");
			}
		}
		return $tree;
	}
	
	function print_preview()	
	//prints a preview of this template only (without processing
	//subtemplates), for the benefit of the designer
	{
		for ($i=0; $i < count($this->content); $i++)
		{
			$line = $this->content[$i];
			$line = WebApp::replaceVars($line);
			print "\t\t".$line;
		}
	}

	/*************************************************************/
	function toHtml()
	//prints the template, replacing all variables
	//and including all subtemplates
	{
		global $webPage, $tplVars;

		$tplVars->enterNewScope();
		$tplVars->addVars($this->getVars());
		
		//add the variable {{./}} which contains the current path
		$tpl_path = dirname($this->filename) . "/";
		$tpl_path = WebApp::to_url_path($tpl_path);
		$tplVars->addVar("./", $tpl_path);
		
		$i = 0;
		while ($i < sizeof($this->content))
		{
			$line = $this->content[$i++];
			if ( chop($line)=="##" )	//subtemplate indicator found
			{
				$line = $this->content[$i++];
				$tpl_id = trim($line);
				$tpl = $webPage->getTemplate($tpl_id);
				$tpl->toHtml();
			}
			else
			{
				//search for vars: {{var_name}} and replace them
				$line = WebApp::replaceVars($line);
				//output the line
				$webPage->print_line($line);
			}
		}
		
		$tplVars->leaveScope();
	}
}

?>