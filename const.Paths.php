<?
//constants for the paths of the application framework

//components paths
define("CONSTRUCTOR_PATH",	WEBAPP_PATH."page_constructor/");
define("SESSION_PATH",		WEBAPP_PATH."session/");
define("DB_PATH",			WEBAPP_PATH."database/");
define("TIMER_PATH",		WEBAPP_PATH."timer/");

//WebBox paths
define("WEBBOX_PATH",		WEBAPP_PATH."boxes/");
define("FOLDERLISTING_PATH",	WEBBOX_PATH."folderListing/");
define("FOLDERMANAGER_PATH",	WEBBOX_PATH."folderManager/");
define("EDITMENUS_PATH",	WEBBOX_PATH."editMenus/");
define("FILEVIEW_PATH",		WEBBOX_PATH."fileView/");
define("CODEVIEWER_PATH",	WEBBOX_PATH."codeViewer/");

//tools paths
define("TOOLS_PATH",		WEBAPP_PATH."tools/");
define("BROWSER_PATH",		TOOLS_PATH."fileBrowser/");
?>
