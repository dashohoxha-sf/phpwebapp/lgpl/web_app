
function on_path(path)
{
	GoTo("thisPage?event=any.path(path="+path+")");
}

function on_folderUp()
{
	GoTo("thisPage?event=any.up");
}

function on_folderRoot()
{
	GoTo("thisPage?event=any.root");
}

function on_folder($folder_name)
{
	GoTo("thisPage?event=any.folder(folder_name="+$folder_name+")");
}

function on_file($file_name)
{
	GoTo("thisPage?event=any.file(file_name="+$file_name+")");
}
