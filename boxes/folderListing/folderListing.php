<?
function folderListing_eventHandler($event)
{
	global $session;
	
	switch ($event->name)
	{
		case "changeRoot":
			$root_folder = $event->args["root_folder"];
			if (substr($root_folder, -1)<>"/")	$root_folder .= "/";
			$session->Vars["folderListing.root"] = $root_folder;
			$session->Vars["folderListing.currentPath"] = "";
			break;
			
		case "path":
			$current_path = $event->args["path"];
			$session->Vars["folderListing.currentPath"] = $current_path;
			break;
			
		case "up":
			$current_path = $session->Vars["folderListing.currentPath"];
			$current_path = ereg_replace("[^/]*/$", "", $current_path);
			$session->Vars["folderListing.currentPath"] = $current_path;
			break;
			
		case "root":
			$session->Vars["folderListing.currentPath"] = "";
			break;
			
		case "folder":
			$folder_name = $event->args["folder_name"];
			$current_path = $session->Vars["folderListing.currentPath"];
			$current_path .= $folder_name."/";
			$session->Vars["folderListing.currentPath"] = $current_path;
			break;
			
		case "file":
			$file_name = $event->args["file_name"];
			$session->Vars["folderListing.selectedFile"] = $file_name;
			break;
	}
}

function folderListing_onLoad()
{
	global $session;
	
	if (!isset($session->Vars["folderListing.root"]))
	{
		$session->AddVar("folderListing.root", APP_PATH);
	}
	
	if (!isset($session->Vars["folderListing.currentPath"]))
	{
		$session->AddVar("folderListing.currentPath", "");
	}
	
	if (!isset($session->Vars["folderListing.fileFilter"]))
	{
		$session->AddVar("folderListing.fileFilter", "html?$");
	}
}

function folderListing_onRender()
//add the variables {{linkedPath}} and {{htmlCode}}
//that are used in the 'folderListing' WebBox
{
	global $session;

	$root = $session->Vars["folderListing.root"];
	$path = $session->Vars["folderListing.currentPath"];
	$filter = $session->Vars["folderListing.fileFilter"];
	
	//add the linkedPath variable
	$linkedPath = folderListing_makeLinkedPath($path);
	WebApp::addVar("linkedPath", $linkedPath);
	
	//add the htmlCode variable
	list($arrFolders,$arrFiles) = folderListing_readFolder($root.$path, $filter);
	$htmlFolderListing = folderListing_toHtml($arrFolders, $arrFiles);
	WebApp::addVar("htmlCode", $htmlFolderListing);
}

function folderListing_readFolder($folder_path, $filter)
//returns an array of folders and an array of files that 
//are in the given $folder_path (the files are filtered by $filter)
{
	if (filetype($folder_path) <> 'dir')
	{
	    print WebApp::error_msg("'$folder_path' is not a folder.");
	    return;
	}

	$dir = opendir($folder_path);
	if (!$dir)
	{
		print WebApp::error_msg("Cannot open folder '$folder_path'.");
		return;
	}

	if ( substr($folder_path, -1) <> "/" )	$folder_path .= "/";
	while ($fname = readdir($dir))
	{
		if (filetype($folder_path.$fname) == 'dir')
		{
			$arrFolders[] = $fname;
		}
		else if (filetype($folder_path.$fname) == 'file')
		{
			if ( eregi($filter,$fname) )
			{
				$arrFiles[] = $fname;
			}
		}
	}
	closedir($dir);
	return array($arrFolders, $arrFiles);
}

function folderListing_toHtml($arrFolders, $arrFiles)
//takes an array of folder names and an array of file names
//and builds an html code that represents a folder listing
{
	$html = '<a href="javascript: on_folderUp()"><span class="folderListing_up">Up</span></a>'."\n";
	$html .= '<a href="javascript: on_folderRoot()"><span class="folderListing_up">Root</span></a><br>'."\n";
	for ($i=0; $i < sizeof($arrFolders); $i++)
	{
		$folder = $arrFolders[$i];
		if ($folder=="." or $folder==".." or $folder=="CVS")	continue;
		$html .= '<a href="javascript: on_folder(' . "'$folder'" . ')"><span class="folderListing_folder">[+] '.$folder.'</span></a><br>'."\n";
	}
	for ($i=0; $i < sizeof($arrFiles); $i++)
	{
		$fname = $arrFiles[$i];
		$html .= '<a href="javascript: on_file(' . "'$fname'" . ')"><span class="folderListing_file">[-] '.$fname.'</span></a><br>'."\n";
	}
	$html .= "</xmp>\n";
	return $html;
}

function folderListing_makeLinkedPath($path)
//$path is a string like this: "folder1/folder2/folder3/"
{
	$folders = explode("/", $path);
	$linked_path = '<a href="javascript: on_path(\'\')"><span class="folderListing_path">Root</span></a>&gt;';
	$current_path = "";
	for ($i=0; $i < sizeof($folders) - 2; $i++)
	{
		$current_path .= $folders[$i] . "/";
		$linked_path .= '<a href="javascript: on_path('."'$current_path'".')"><span class="folderListing_path">'.$folders[$i].'</span></a>&gt;';
	}
	if (sizeof($folders) > 1)
	{
		$last_folder = $folders[sizeof($folders)-2];
		$linked_path .= "$last_folder\n";
	}
	return $linked_path;
}
?>