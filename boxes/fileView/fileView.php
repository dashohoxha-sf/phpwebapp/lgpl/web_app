<?

function fileView_onLoad()
{
	global $session;

	//usually the file type is deducted from the extension of the
	//file, but if the session variable 'fileView_type' is set,
	//it overrides the extension
	if ( isset($session->Vars["fileView_type"]) )
	{
		$file_type = $session->Vars["fileView_type"];
	}
	else
	{
		$file_name = $session->Vars["fileView_file"];
		ereg("(\.[^.]*)$", $file_name, $regs);
		$ext = $regs[1];
		switch ($ext)
		{
			case ".html":
			case ".htm":
				$file_type = "Html";
				break;
			case ".gif":
			case ".jpg":
				$file_type = "Image";
				break;
			case ".txt":
				$file_type = "Text";
				break;
			case ".php":
			case ".php3":
			case ".php4":
				$file_type = "PHP";
				break;
			case ".js":
				$file_type = "JS";
				break;
			case ".css":
				$file_type = "CSS";
				break;
			default:
				$file_type = "Unknown";
				break;
		}
	}

	$path = WebApp::getVar("./");
	switch ($file_type)
	{
		case "PHP":
			//PHP is always displayed as code
			$template = $path."viewPHP.html";
			break;

		case "Image":
			//Images are always displayed in preview mode
			$template = $path."viewImage.html";
			break;

		case "JS":
		case "CSS":
		case "Text":
			//Text is always displayed in code view mode
			$file_name = $session->Vars["fileView_file"];
			$session->Vars["codeViewer_codeFile"] = $file_name;
			$template = CODEVIEWER_PATH."codeViewer.html";
			//$template = $path."viewText.html";
			break;

		case "Html":
			//Html is displayed either in code view
			//or in preview mode, depending on 'codeView' variable
			$codeView = $session->Vars["codeView"];
			if ($codeView=="true")
			{
				$file_name = $session->Vars["fileView_file"];
				$session->Vars["codeViewer_codeFile"] = $file_name;
				$template = CODEVIEWER_PATH."codeViewer.html";
			}
			else
			{
				$template = $path."viewHtml.html";
			}
			break;

		default:
		case "Unknown":
			$template = $path."viewUnknown.html";
			break;
	
	}
	//set the template that will be used to display the file
	WebApp::addVar("fileView_template", $template);
}

?>
