<?
function viewImage_onRender()
{
	global $session;

	$file_path = $session->Vars["fileView_file"];
	$img_url = WebApp::to_url_path($file_path);
	WebApp::addVar("image_src", $img_url);
}
?>
