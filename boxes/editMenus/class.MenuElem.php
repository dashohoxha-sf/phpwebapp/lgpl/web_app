<?
if ( !defined("class_MenuElem") )
//test so that it is not included multiple times
{
define("class_MenuElem", 1);

/////////////////////////////////////////////////////
//	Class MenuElem represents an element of a menu.
//
class MenuElem
{
	var $id;			//identifies this menu element
	var $description;	//text that will be shown in the menu
	var $action;		//what will be done when this element is selected (typically a JS function call)
	var $parent;		//parent of this element in the hierarchy of the menu
	var $children;		//array of subelements

	function MenuElem($id ="undefined", $descr ="menu element", $act ="")
	{
		$this->id = $id ;
		$this->description = $descr;
		$this->action = $act;
		$this->children = array();
	}
	
	function build_from_dom($elem)
	//builds a menu element from a DOMXML node
	{
		//get the attributes
		$this->id				= $elem->getattr("id");
		$this->description		= $elem->getattr("description");
		$this->action			= $elem->getattr("action");
		if ($this->description=="")		$this->description = $this->id;
		if ($this->action=="")			$this->action = $this->id;

		//add the child elements
		$children = $elem->children();
		while ( list($i,$child) = each($children) )
		{
			if ($child->name=="MenuElem")
			{
				$sub_elem = new MenuElem;
				$sub_elem->build_from_dom($child);
				$this->addChild($sub_elem);
			}
		}
	}

	function addChild(&$ch)
	{
		$this->children[] = $ch;
		$ch->parent = &$this;
	}
	
	function nrChildren()
	{
		return sizeof($this->children);
	}

	function hasChildren()
	{
		$nr_ch = $this->nrChildren();
		return ($nr_ch <> 0);
	}

	function set_children_id()
	//set the ids of the children (if unset)
	//used by the function that loads the menu from an xml file
	{
		for ($i=0; $i < $this->nrChildren(); $i++)
		{
			$child = &$this->children[$i];
			if ($child->id=="undefined")
			{
				$child->id = $this->id . "_" . ($i+1);
			}
			$child->set_children_id();
		}
	}
	
	function new_child_id()
	//returns an id for a new child
	{
		$max_id = $this->id."_0";
		for ($i=0; $i < $this->nrChildren(); $i++)
		{
			$id = $this->children[$i]->id;
			if ($id > $max_id)	$max_id = $id;
		}
		$max_id++;
		return $max_id;
	}
	
	function setEditAction()
	//sets the action that is used when the menu is being edited
	{
		$id = $this->id;
		$descr = $this->description;
		$action = $this->action;
		$this->action = "edit(\\\"$id\\\", \\\"$descr\\\", \\\"$action\\\")";
		for ($i=0; $i < $this->nrChildren(); $i++)
		{
			$child = &$this->children[$i];
			$child->setEditAction();
		}
	}
	
	function change($id, $change_func)
	{
		for ($i=0; $i < $this->nrChildren(); $i++)
		{
			$child = &$this->children[$i];
			if ($child->id==$id)	
			{
				$change_func($this, $child);
				return true;
			}
			$changed = $child->change($id, $change_func);
			if ($changed) return true;
		}
		
		return false;
	}

	/////////// output functions /////////////
	
	function to_xml($indent)
	//returns the menu element as an xml string indented by $indent
	{
		$xml_menu = $indent.'<MenuElem id="'.$this->id."\"\n"
					. "\t\t".$indent.'description="'.$this->description."\"\n"
					. "\t\t".$indent.'action="'.$this->action."\">\n";
		$xml_menu .= $this->children_to_xml("\t".$indent);
		$xml_menu .= $indent."</MenuElem>\n";
		return $xml_menu;
	}
	
	function children_to_xml($indent)
	//returns the children as an xml string indented by $indent
	{
		for ($i=0; $i < $this->nrChildren(); $i++)
		{
			$child = $this->children[$i];
			$xml_menu .= $child->to_xml($indent);
		}
		return $xml_menu;
	}

	function to_text($ident)		//debug
	//returns the element and all its subelements
	//in an easily readable text format
	{
		$txt_menu = $ident."> ".$this->id." -> ".$this->description." -> ".$this->action."\n";
		for ($i=0; $i < $this->nrChildren(); $i++)
		{
			$child = $this->children[$i];
			$txt_menu .= $child->to_text("\t".$ident);
		}
		return $txt_menu;
	}
	
	function print_text($ident)		//debug
	//prints the element and all its subelements
	//in an easily readable text format
	{
		print $ident."> ".$this->id." -> ".$this->description." -> ".$this->action."\n";
		for ($i=0; $i < $this->nrChildren(); $i++)
		{
			$child = $this->children[$i];
			print $child->print_text("\t".$ident);
		}
	}

	function to_arrMenus($menu_name)
	//returns the branch starting with this element
	//in the JavaScript format required by hierMenus
	{
		if ( !$this->hasChildren() )	return "";

		$js_menu = $menu_name." = new Array(\n";
		$nrCh = $this->nrChildren();
		for ($i=0; $i < $nrCh-1; $i++)
		{
			$ch = $this->children[$i];
			$js_menu .= "\t\"" . $ch->description . "\", \"" . $ch->action."\", "
						. ($ch->hasChildren() ? "1" : "0") . ",\n";
		}
		//last child
		$ch = $this->children[$i];
		$js_menu .= "\t\"" . $ch->description . "\", \"" . $ch->action."\", "
					. ($ch->hasChildren() ? "1" : "0") . "\n";	//without a comma
		$js_menu .= ");\n\n";
		$js_menu .= $this->children_to_arrMenus($menu_name."_");

		return $js_menu;
	}

	function children_to_arrMenus($menu_name)
	//transforms all the children to JavaScript code
	//and returns it as a string, used by to_arrMenus() etc.
	{
		for ($i=0; $i < $this->nrChildren(); $i++)
		{
			$ch = &$this->children[$i];
			$ch_menu_name = $menu_name . ($i + 1);
			$js_menu .= $ch->to_arrMenus($ch_menu_name);
		}
		return $js_menu;
	}
 }

}	//end if !defined
?>
