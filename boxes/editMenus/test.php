<?
//include "../../../application.php";
define("EDITMENUS_PATH", "");
include EDITMENUS_PATH."class.Menus.php";

$menus = new Menus("Personal");
$menus->read_xml(EDITMENUS_PATH."sample_menu/sample_Menus.xml");

$menus->change("Personal_1_1_1", "addChildren");

print $menus->to_text("@@");
//print $menus->to_xml();
//$menus->write_xml(EDITMENUS_PATH."sample_menu/menus_1.xml");

//print buildMenuBar();

//$menus->setEditAction();
//print $menus->to_arrMenus();


/*----------------------------------------------------*/

function change_id(&$parent, &$elem)
{
	$elem->id = "XXXXXX";
}

function moveUp(&$parent, &$elem)
{
	$i = find_position(&$parent, &$elem);
	if ($i==-1 or $i=0)	return;
	
	$prev_child = &$parent->children[$i-1];
	//switch the places of child and the child before it
	$temp = $prev_child;
	$prev_child = $child;
	$child = $temp;
}

function moveDown(&$parent, &$elem)
{
	$nr = $parent->nrChildren();
	$i = find_position(&$parent, &$elem);
	if ($i==-1 or $i==($nr-1))	return;
	
	$next_child = &$parent->children[$i+1];
	//switch the places of child and the next child
	$temp = $next_child;
	$next_child = $child;
	$child = $temp;
}

function update(&$parent, &$elem)
{
	global $id, $descr, $action;
	
	$id = "XXXXXXXX";
	$descr = "YYYYYYYY";
	$action = "ZZZZZZZZ";
	
	$elem->id = $id;
	$elem->description = $descr;
	$elem->action = $action;
}

function delete(&$parent, &$elem)
{
	$i = find_position(&$parent, &$elem);
	if ($i==-1)	return;
	
	//shift the rest of the children by one position
	$nr = $parent->nrChildren();
	while ($i < $nr-1)
	{
		$child = &$parent->children[$i];
		$next_child = &$parent->children[$i+1];
		//$parent->children[$i] = &$parent->children[$i+1];
		$child = $next_child;
		$i++;
	}
	//delete the last element of the children
	unset($parent->children[$nr-1]);
}

function addBefore(&$parent, &$elem)
{
	$new_elems[] = new MenuElem("11111","11111","11111");
	$new_elems[] = new MenuElem("22222","22222","22222");
	$new_elems[] = new MenuElem("33333","33333","33333");
	
	$pos = find_position(&$parent, &$elem);
	if ($pos==-1)	return;
	
	//copy the $parent->children to the array $children
	for ($i=0; $i<sizeof($parent->children); $i++)
	{
		$ch = &$parent->children[$i];
		$children[] = $ch;
	}
	//empty the array $parent->children
	$parent->children = array();

	//add the elements before the selected one
	for ($i=0; $i<$pos; $i++)
	{
		$ch = &$children[$i];
		$parent->children[] = $ch;
	}
	
	//add the new elements as the childs of the $parent
	for ($j=0; $j < sizeof($new_elems); $j++)
	{
		$ch = $new_elems[$j];
		$parent->addChild($ch);
	}
	
	//add the elements after the selected one
	$nr = sizeof($children);
	for ($i=$pos; $i<$nr; $i++)
	{
		$ch = &$children[$i];
		$parent->children[] = $ch;
	}
}

function addAfter(&$parent, &$elem)
{
	$new_elems[] = new MenuElem("11111","11111","11111");
	$new_elems[] = new MenuElem("22222","22222","22222");
	$new_elems[] = new MenuElem("33333","33333","33333");

	$pos = find_position(&$parent, &$elem);
	if ($pos==-1)	return;
	
	//copy the $parent->children to the array $children
	for ($i=0; $i<sizeof($parent->children); $i++)
	{
		$ch = &$parent->children[$i];
		$children[] = $ch;
	}
	//empty the array $parent->children
	$parent->children = array();

	//add the elements before the selected one
	for ($i=0; $i<=$pos; $i++)
	{
		$ch = &$children[$i];
		$parent->children[] = $ch;
	}
	
	//add the new elements as the children of the $parent
	for ($j=0; $j < sizeof($new_elems); $j++)
	{
		$ch = $new_elems[$j];
		$parent->addChild($ch);
	}
	
	//add the elements after the selected one
	$nr = sizeof($children);
	for ($i=$pos+1; $i<$nr; $i++)
	{
		$ch = &$children[$i];
		$parent->children[] = $ch;
	}	
}

function addChildren(&$parent, &$elem)
{
	$new_elems[] = new MenuElem("11111","11111","11111");
	$new_elems[] = new MenuElem("22222","22222","22222");
	$new_elems[] = new MenuElem("33333","33333","33333");

	//add the new elements as the children of the $elem
	for ($j=0; $j < sizeof($new_elems); $j++)
	{
		$ch = $new_elems[$j];
		$elem->addChild($ch);
	}
}

function find_position(&$parent, &$child)
//finds the position of the $child 
//among the children of the $parent
//if not found, returns -1
{
	$nr = $parent->nrChildren();
	for ($i=0; $i < $nr; $i++)
	{
		$ch = &$parent->children[$i];
		if ($ch->id == $child->id)	return $i;
	}
	return -1;
}

/*----------------------------------------------------*/

function buildMenuBar()
//returns the variable {{MENU_BAR}} which contains 
//the links for the menu top elements
{
	global $menus;
	
	$arr_top_elems = array();
	for ($i=0; $i<$menus->nrChildren(); $i++)
	{
		$top_elem = $menus->children[$i];
		$arr_top_elems[] = $top_elem->description;
	}
	
	while ( list($i,$label) = each($arr_top_elems) )
	{
		$i += 1;
$menu_bar .= "
		<td>
			<a href=\"\"	onMouseOver=\"popUp('elMenu$i',event)\"
						onMouseOut=\"popDown('elMenu$i')\"
						onClick = \"return false;\">
				$label
			</a>
		</td>";
	}
	
	return $menu_bar;
}

?>