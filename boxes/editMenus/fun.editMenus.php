<?

function editMenus_rewriteFiles()
{
	global $session, $menus;
	
	$menu_path = $session->Vars["menu_data_path"];
	
	//write the menu_data.xml file
	$menu_xml_file = $menu_path."menu_data.xml";
	$menus->write_xml($menu_xml_file);
		
	//write the menu_arrays.js file
	$menu_js_file = $menu_path."menu_arrays.js";
	$fp = fopen($menu_js_file, "w");
	fwrite($fp, $menus->to_arrMenus());
	fclose($fp);
	
	//write the menubar.html file
	$menubar_file = $menu_path."menubar.html";
	$fp = fopen($menubar_file, "w");
	fwrite($fp, $menus->to_HTML_table());
	fclose($fp);
}

function editMenus_findPosition(&$parent, &$child)
//finds the position of the $child 
//among the children of the $parent
//if not found, returns -1
{
	$nr = $parent->nrChildren();
	for ($i=0; $i < $nr; $i++)
	{
		$ch = &$parent->children[$i];
		if ($ch->id == $child->id)	return $i;
	}
	return -1;
}

function editMenus_moveUp(&$parent, &$elem)
{
	$i = editMenus_findPosition(&$parent, &$elem);
	if ($i==-1 or $i==0)	return;
	
	$child		= &$parent->children[$i];
	$prev_child	= &$parent->children[$i-1];
	
	//switch the places of child and the child before it
	$temp = $prev_child;
	$prev_child = $child;
	$child = $temp;
}

function editMenus_moveDown(&$parent, &$elem)
{
	$nr = $parent->nrChildren();
	$i = editMenus_findPosition($parent,$elem);
	if ($i==-1 or $i==($nr-1))	return;
	
	$child		= &$parent->children[$i];
	$next_child	= &$parent->children[$i+1];
	
	//switch the places of child and the next child
	$temp = $next_child;
	$next_child = $child;
	$child = $temp;
}

function editMenus_update(&$parent, &$elem)
{
	global $event;
	
	list($id,$descr,$action) = $event->getArgs();
	$elem->id = $id;
	$elem->description = $descr;
	$elem->action = $action;
}

function editMenus_delete(&$parent, &$elem)
{
	$i = editMenus_findPosition(&$parent, &$elem);
	if ($i==-1)	return;
	
	//shift the rest of the children by one position
	$nr = $parent->nrChildren();
	while ($i < $nr-1)
	{
		$child = &$parent->children[$i];
		$next_child = &$parent->children[$i+1];
		//$parent->children[$i] = &$parent->children[$i+1];
		$child = $next_child;
		$i++;
	}
	//delete the last element of the children
	unset($parent->children[$nr-1]);
}

function editMenus_getNewElems()
//returns an array of the new elements to be added
{
	global $event;
	extract($event->args);

	$new_elems = array();
	$arr_descr = split("\n", $descr_list);
	$arr_action = split("\n", $action_list);
	for ($i=0; $i < sizeof($arr_descr); $i++)
	{
		$descr = trim($arr_descr[$i]);
		$action = trim($arr_action[$i]);
		if ($descr <> "")
		{
			$elem = new MenuElem("undefined", $descr, $action);
			$new_elems[] = $elem;
		}
	}
		
	return $new_elems;
}

function editMenus_addBefore(&$parent, &$elem)
{	
	$pos = editMenus_findPosition(&$parent, &$elem);
	if ($pos==-1)	return;
	
	//get the id for the new elements
	$new_id = $parent->new_child_id();
	
	//copy the $parent->children to the array $children
	for ($i=0; $i<sizeof($parent->children); $i++)
	{
		$ch = &$parent->children[$i];
		$children[] = $ch;
	}
	//empty the array $parent->children
	$parent->children = array();

	//add the elements before the selected one
	for ($i=0; $i<$pos; $i++)
	{
		$ch = &$children[$i];
		$parent->children[] = $ch;
	}
	
	//add the new elements as the childs of the $parent
	$new_elems = editMenus_getNewElems();
	for ($j=0; $j < sizeof($new_elems); $j++)
	{
		$ch = $new_elems[$j];
		$ch->id = $new_id++;
		$parent->addChild($ch);
	}
	
	//add the elements after the selected one
	$nr = sizeof($children);
	for ($i=$pos; $i<$nr; $i++)
	{
		$ch = &$children[$i];
		$parent->children[] = $ch;
	}
}

function editMenus_addAfter(&$parent, &$elem)
{
	$pos = editMenus_findPosition(&$parent, &$elem);
	if ($pos==-1)	return;

	//get the id for the new elements
	$new_id = $parent->new_child_id();
		
	//copy the $parent->children to the array $children
	for ($i=0; $i<sizeof($parent->children); $i++)
	{
		$ch = &$parent->children[$i];
		$children[] = $ch;
	}
	//empty the array $parent->children
	$parent->children = array();

	//add the elements before the selected one
	for ($i=0; $i<=$pos; $i++)
	{
		$ch = &$children[$i];
		$parent->children[] = $ch;
	}
	
	//add the new elements as the children of the $parent
	$new_elems = editMenus_getNewElems();
	for ($j=0; $j < sizeof($new_elems); $j++)
	{
		$ch = $new_elems[$j];
		$ch->id = $new_id++;
		$parent->addChild($ch);
	}
	
	//add the elements after the selected one
	$nr = sizeof($children);
	for ($i=$pos+1; $i<$nr; $i++)
	{
		$ch = &$children[$i];
		$parent->children[] = $ch;
	}	
}

function editMenus_addChildren(&$parent, &$elem)
{
	//add the new elements as the children of the $elem
	$new_elems = editMenus_getNewElems();
	$new_id = $elem->new_child_id();
	for ($i=0; $i < sizeof($new_elems); $i++)
	{
		$ch = $new_elems[$i];
		$ch->id = $new_id++;
		$elem->addChild($ch);
	}
}
?>
