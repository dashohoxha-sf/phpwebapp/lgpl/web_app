<?

class XMLMenus
{
	var $root;			//root element of the tree
	var $stack;			//stack of elements in processing order
	var $currentElement;

	function XMLMenus($file_name)
	{
		$this->stack = array();
		$this->currentElement = new MenuElem;
		$this->parse($file_name);
		$this->root = $this->currentElement->children[0];
	}
	
	function getMenus()
	{
		return $this->root;
	}

	function parse($file_name)
	{
		$xml_parser = xml_parser_create();

		xml_set_object($xml_parser, &$this);	//this works only on PHP4
		xml_set_element_handler($xml_parser, "startElementHandler", "endElementHandler");
		xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, 0);

		if (!($fp = fopen($file_name, "r")))
		{
			die("could not open XML input");
		}
		while ($data = fread($fp, 4096))
		{
			if (!xml_parse($xml_parser, $data, feof($fp)))
			{
				die(sprintf("XML error: %s at line %d",
				xml_error_string(xml_get_error_code($xml_parser)),
				xml_get_current_line_number($xml_parser)));
			}
		}
		xml_parser_free($xml_parser);
	}

	function startElementHandler($parser, $name, $attrs)
	{
		extract($attrs);
		switch ($name)
		{
			case "MenuElem":
				$elem = new MenuElem;
				$elem->id			= $id;
				$elem->description	= $description;
				$elem->action		= $action;
				break;
				
			case "MenuTopElem":
				$elem = new MenuTopElem;
				$elem->id			= $id;
				$elem->description	= $description;
				$elem->action		= $action;
				$elem->left_position= $leftPos;
				$elem->top_position	= $topPos;
				break;
				
			case "Menus":
				$elem = new Menus("Menus");
				$elem->id					= $id;
				$elem->description			= $description;
				$elem->action				= $action;
				$elem->width				= $width;
				$elem->font_color			= $fontColor;
				$elem->mouseover_font_color	= $mouseoverFontColor;
				$elem->bg_color				= $bgColor;
				$elem->mouseover_bg_color	= $mouseoverBgColor;
				$elem->border_color			= $borderColor;
				$elem->separator_color		= $separatorColor;
				break;
		}
		
		array_push($this->stack, &$this->currentElement);
		$this->currentElement = &$elem;
	}

	function endElementHandler($parser, $name)
	{
		$elem = &$this->currentElement;
		$this->currentElement = &array_pop($this->stack);
		$this->currentElement->addChild(&$elem);
	}

}

?>
