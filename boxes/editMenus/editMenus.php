<?

function editMenus_eventHandler($event)
{			
	global $session;
	if ( !isset($session->Vars["menu_data_path"]) )
	{
		$session->Vars["menu_data_path"] = EDITMENUS_PATH."sample_menu/menu_data/";
	}

	switch ($event->name)
	{
		case "moveUp":
		case "moveDown":
		case "update":
		case "delete":
		case "addBefore":
		case "addAfter":
		case "addChildren":
			include EDITMENUS_PATH."fun.editMenus.php";
			global $menus;
			editMenus_buildMenus();	
			$id = $event->args["menu_id"];
			$menus->change($id, "editMenus_".$event->name);
			editMenus_rewriteFiles();
			break;
	}
}

function edirMenus_onLoad()
{
	global $session;
	
	if ( !isset($session->Vars["menu_data_path"]) )
	{
		$session->Vars["menu_data_path"] = EDITMENUS_PATH."sample_menu/menu_data/";
	}
}

function editMenus_onRender()
{		
	global $session, $menus;
	
	if ( !isset($menus) )		editMenus_buildMenus();
	$menus->setEditAction();
	
	WebApp::addVar("MENUBAR", $menus->to_HTML_table() );
	WebApp::addVar("EDIT_ARRMENUS", $menus->to_arrMenus());
}

function editMenus_buildMenus()
//builds the global variable $menus
{
	global $session, $menus;
	
	include EDITMENUS_PATH."class.Menus.php";
	
	$menu_path = $session->Vars["menu_data_path"];
	$menu_xml_file = $menu_path."menu_data.xml";
	
	/* 
	//This works only when the PHP is configured with DOM XML
	$menus = new Menus("Menus");
	$menus->read_xml($menu_xml_file);
	*/
	
	//Uses the XML extention of PHP, requires PHP4
	include EDITMENUS_PATH."class.XMLMenus.php";
	$xmlMenus = new XMLMenus($menu_xml_file);
	$menus = $xmlMenus->getMenus();
}

?>