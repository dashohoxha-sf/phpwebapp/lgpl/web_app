
function edit(id, description, action)
{
	var form = document.editMenusForm;

	form.id.value = id;
	form.description.value = description;
	form.action.value = action;
}

function on_moveUp()
{
	var form = document.editMenusForm;
	var id = form.id.value;
	
	if (id=="")
	{
		alert("No menu item selected!");
	}
	else
	{
		GoTo("thisPage?event=editMenus.moveUp(menu_id="+id+")");
	}
}

function on_moveDown()
{
	var form = document.editMenusForm;
	var id = form.id.value;
	
	if (id=="")
	{
		alert("No menu item selected!");
	}
	else
	{
		GoTo("thisPage?event=editMenus.moveDown(menu_id="+id+")");
	}
}

function on_update()
{
	var form = document.editMenusForm;
	var id = form.id.value;
	var descr = form.description.value;
	var action = form.action.value;
	
	if (id=="")
	{
		alert("No menu item selected!");
	}
	else
	{
		GoTo("thisPage?event=editMenus.update(menu_id="+id+";"
					+"descr="+descr+";"
					+"action="+action+")");
	}
}

function on_delete()
{
	var form = document.editMenusForm;
	var id = form.id.value;
	
	if (id=="")
	{
		alert("No menu item selected!");
	}
	else
	{
		GoTo("thisPage?event=editMenus.delete(menu_id="+id+")");
	}
}

function on_addBefore()
{
	var form = document.editMenusForm;
	var id = form.id.value;
	var descr_list = form.description_list.value;
	var action_list = form.action_list.value;

	if (id=="")
	{
		alert("No menu item selected!");
	}
	else
	{
		GoTo("thisPage?event=editMenus.addBefore(menu_id="+id+";"
						+"descr_list="+descr_list+";"
						+"action_list="+action_list+")");
	}
}

function on_addAfter()
{
	var form = document.editMenusForm;
	var id = form.id.value;
	var descr_list = form.description_list.value;
	var action_list = form.action_list.value;
	
	if (id=="")
	{
		alert("No menu item selected!");
	}
	else
	{
		GoTo("thisPage?event=editMenus.addAfter(menu_id="+id+";"
						+"descr_list="+descr_list+";"
						+"action_list="+action_list+")");
	}
}

function on_addChildren()
{
	var form = document.editMenusForm;
	var id = form.id.value;
	var descr_list = form.description_list.value;
	var action_list = form.action_list.value;
	
	if (id=="")
	{
		alert("No menu item selected!");
	}
	else
	{
		GoTo("thisPage?event=editMenus.addChildren(menu_id="+id+";"
						+"descr_list="+descr_list+";"
						+"action_list="+action_list+")");
	}
}
