<?
include EDITMENUS_PATH."class.MenuElem.php";
include EDITMENUS_PATH."class.MenuTopElem.php";

/////////////////////////////////////////////////////
//	Class Menu represents all the menus of a page.
//
class Menus extends MenuElem
{
	//style of the menus
	var $width;					//width of a menu element
	var $font_color;
	var $mouseover_font_color;
	var $bg_color;
	var $mouseover_bg_color;
	var $border_color;
	var $separator_color;

	function Menus($id ="Menus", $descr ="Menus")
	{
		MenuElem::MenuElem($id, $descr);

		//default values for the outlook of the menus
		$this->width				= "150";
		$this->font_color			= "#000000";
		$this->mouseover_font_color	= "#FFFFFF";
		$this->bg_color				= "#F0E8D0";
		$this->mouseover_bg_color	= "#336699";
		$this->border_color			= "#003366";
		$this->separator_color		= "#003366";
	}
	
	function read_xml($fname)
	//builds a menu by reading an XML file
	{
		if (!file_exists($fname))
		{
			print "Error: file '$fname' cannot be found.\n";
			return;
		}
		//$doc = xmldocfile($fname);	//bug: the file name exists, but it cannot open it
		$arr_lines = file($fname);
		$str_lines = implode("",$arr_lines);
		$doc = xmldoc($str_lines);
		$root = $doc->root();
		
		//get the attributes of the <Menus> element
		$this->id					= $root->getattr("id");
		$this->width				= $root->getattr("width");
		$this->font_color			= $root->getattr("fontColor");
		$this->mouseover_font_color	= $root->getattr("mouseoverFontColor");
		$this->bg_color				= $root->getattr("bgColor");
		$this->mouseover_bg_color	= $root->getattr("mouseoverBgColor");
		$this->border_color			= $root->getattr("borderColor");
		$this->separator_color		= $root->getattr("separatorColor");

		//read the menu top elements
		$children = $root->children();
		while ( list($i,$child) = each($children) )
		{
			if ($child->name=="MenuTopElem")
			{
				$menu_top_elem = new MenuTopElem;
				$menu_top_elem->build_from_dom($child);
				$this->addChild($menu_top_elem);
			}
		}
		//$this->set_children_id();
	}
	
	function write_xml($fname)
	//writes the menus as an XML file
	{
		$fp = fopen($fname, "w");
		fwrite($fp, $this->to_xml());
		fclose($fp);
	}
	
	function to_xml()
	//returns the menus as an XML string
	{
		$xml_menu = "<?xml version=\"1.0\"?>
<!DOCTYPE Menus SYSTEM \"menus.dtd\">

<Menus id=\"$this->id\"	
		width=\"$this->width\" 
		fontColor=\"$this->font_color\" 
		mouseoverFontColor=\"$this->mouseover_font_color\" 
		bgColor=\"$this->bg_color\" 
		mouseoverBgColor=\"$this->mouseover_bg_color\" 
		borderColor=\"$this->border_color\" 
		separatorColor=\"$this->separator_color\">

";
		$xml_menu .= $this->children_to_xml("\t");
		$xml_menu .= "</Menus>";
		return $xml_menu;
	}

	function to_arrMenus()
	//returns the JavaScript code for all the menus of the page
	//in the format required by the hierMenus
	{
		$idx = 0;
		for ($i=0; $i < $this->nrChildren(); $i++)
		{
			$top_menu = &$this->children[$i];
			if ( !$top_menu->hasChildren() ) continue;
			$idx++;
			$top_menu_name = "arMenu" . $idx;
			$js_menu .= $top_menu->to_arrMenus($top_menu_name, $this->width,
									$this->font_color, $this->mouseover_font_color,
									$this->bg_color, $this->mouseover_bg_color,
									$this->border_color, $this->separator_color);
		}
		return $js_menu;
	}
	
	function to_HTML_table()
	//returns an HTML table (as a string) which contains 
	//the links for the menu top elements
	{		
		$menu_bar = "<link rel='stylesheet' type='text/css' href='{{./}}menubar.css'>\n"
					."<table class='menubar_table'>\n"
					."\t<tr class='menubar_tr'>\n";
	
		$idx = 0;
		for ($i=0; $i<$this->nrChildren(); $i++)
		{
			$elem = &$this->children[$i];
			$descr = $elem->description;
			$action = $elem->action;
			$action = str_replace("\\", "", $action);
			if ( !$elem->hasChildren() )
			{
				$menu_bar .= "
				
				<td class='topElem_td'>
					<a href='javascript: $action'>
						<span class='topElem_descr'>$descr</span>
					</a>
				</td>";
			}
			else
			{
				$idx++;
				$menu_bar .= "
				<td class='topElem_td'>
					<a href=\"javascript: $action\" 
								onMouseOver='popUp(\"elMenu$idx\",event)'
								onMouseOut='popDown(\"elMenu$idx\")'>
						<span class='topElem_descr'>$descr</span>
					</a>
				</td>";
			}
				
		}
		
		$menu_bar .= "\t</tr>\n";
		$menu_bar .= "</table>\n";
		
		return $menu_bar;
	}
}

?>
