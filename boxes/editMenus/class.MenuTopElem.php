<?

/////////////////////////////////////////////////////
//	Class MenuTopElem represents a top element of a menu.
//
class MenuTopElem extends MenuElem
{
	var $left_position;
	var $top_position;

	function MenuTopElem($id ="undefined", $descr ="menu top element", $act="",
					$left_position="null", $top_position="null")
	{
		MenuElem::MenuElem($id, $descr, $act);
		$this->left_position	= $left_position;
		$this->top_position		= $top_position;
	}
	
	function build_from_dom($elem)
	//builds a menu top element from a DOMXML node
	{
		//get the attributes
		$this->id				= $elem->getattr("id");
		$this->description		= $elem->getattr("description");
		$this->action			= $elem->getattr("action");
		$this->top_position		= $elem->getattr("topPos");
		$this->left_position	= $elem->getattr("leftPos");
		if ($this->description=="")		$this->description = $this->id;
		if ($this->action=="")			$this->action = $this->id;
		
		//add the child elements
		$children = $elem->children();
		while ( list($i,$child) = each($children) )
		{
			if ($child->name=="MenuElem")
			{
				$sub_elem = new MenuElem;
				$sub_elem->build_from_dom($child);
				$this->addChild($sub_elem);
			}
		}
	}
	
	function to_xml($indent)
	//returns the object as an XML string
	{
		$xml_menu = $indent.'<MenuTopElem id="'.$this->id."\"\n"
					. "\t\t".$indent.'description="'.$this->description."\"\n"
					. "\t\t".$indent.'action="'.$this->action."\"\n"
					. "\t\t".$indent.'topPos="'.$this->top_position."\"\n"
					. "\t\t".$indent.'leftPos="'.$this->left_position."\">\n";
		$xml_menu .= $this->children_to_xml("\t".$indent);
		$xml_menu .= $indent."</MenuTopElem>\n";
		return $xml_menu;
	}

	///////////////////////////////////////////////////
	//returns the menu starting with this top element
	//in the JavaScript format required by hierMenus
	//
	function to_arrMenus($menu_name,
				$width="null",
				$font_color="", $mouseover_font_color="",
				$bg_color="", $mouseover_bg_color="",
				$border_color="", $separator_color="")

	{
		$js_menu = $menu_name . " = new Array(\n"
			. "\t" . $width.",\n"
			. "\t" . $this->left_position	. ", " . $this->top_position . ",\n"
			. "\t\"" . $font_color		. "\", \"" . $mouseover_font_color . "\",\n"
			. "\t\"" . $bg_color		. "\", \"" . $mouseover_bg_color . "\",\n"
			. "\t\"" . $border_color	. "\", \"" . $separator_color . "\",\n";
		
		$nrCh = $this->nrChildren();
		for ($i=0; $i < $nrCh-1; $i++)
		{
			$ch = $this->children[$i];
			$js_menu .= "\t\"" . $ch->description . "\", \"" . $ch->action."\", "
						. ($ch->hasChildren() ? "1" : "0") . ",\n";
		}
		//last child
		$ch = $this->children[$i];
		$js_menu .= "\t\"" . $ch->description . "\", \"" . $ch->action."\", "
					. ($ch->hasChildren() ? "1" : "0") . "\n";	//without a comma
		$js_menu .= ");\n\n";
		$js_menu .= $this->children_to_arrMenus($menu_name."_");
		
		return $js_menu;
	}
}

?>
