
//use these functions until menus are loaded
function popUp() {return};
function popDown(){return};

//Find browser version
   NS4 = (document.layers);
   IE4 = (document.all);
  ver4 = (NS4 || IE4);
 isMac = (navigator.appVersion.indexOf("Mac") != -1);
isMenu = (NS4 || (IE4 && !isMac));

if (!ver4) event = null;

if (!isMenu)
{
	alert("This browser does not support hierMenus!");
}
else
{
	//Set menu parameters.
	menuVersion = 3;

	menuWidth = 120;
	childOverlap = 50;
	childOffset = 5;
	perCentOver = null;
	secondsVisible = 1.5;

	fntCol = "blue";
	fntSiz = "8";
	fntBold = false;
	fntItal = false;
	fntFam = "sans-serif";

	backCol = "#ACACAC";
	overCol = "#FF0000";
	overFnt = "purple";

	borWid = 2;
	borCol = "black";
	borSty = "solid";
	itemPad = 3;

	imgSrc = "{{./}}tri.gif";
	imgSiz = 12;

	separator = 1;
	separatorCol = "red";

	isFrames = false;      	// <-- IMPORTANT for full window
	navFrLoc = "left";  	// <-- display. see below

	keepHilite = true;
	NSfontOver = true;

	clickStart = false;
	clickKill = false;

	//load the arrays of menu data
	document.write("<script language='JavaScript1.2' src='{{menu_data_path}}/menu_arrays.js'><\/script>");

	//load the JS code for the menus
	document.write("<script language='JavaScript1.2' src='{{./}}hierMenus.js'><\/script>");
}
