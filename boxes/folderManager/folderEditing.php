<?
function folderEditing_eventHandler($event)
{
	global $session;
	extract($event->args);
	
	$root = $session->Vars["folderListing.root"];
	$path = $session->Vars["folderListing.currentPath"];
	$currentFolder = $root.$path;
	switch ($event->name)
	{
		case "rename":
			$fromFolder = $currentFolder.$fromFolder;
			$toFolder = $currentFolder.$toFolder;
			rename($fromFolder, $toFolder);
			break;
		case "delete":
			rmdir($currentFolder.$delFolder);
			break;
		case "addNew":
			mkdir($currentFolder.$newFolder, 0700);
			break;
	}
}

function folderEditing_onRender()
{
}
?>