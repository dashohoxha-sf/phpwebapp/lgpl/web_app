
function on_rename()
{
	var form = document.folderEditing_Form;
	var fromFolder = form.fromFolder.value;
	var toFolder = form.toFolder.value;
	GoTo("thisPage?event=folderEditing.rename(fromFolder="+fromFolder
										+";toFolder="+toFolder+")");
}

function on_delete()
{
	var form = document.folderEditing_Form;
	var folder = form.delFolder.value;
	GoTo("thisPage?event=folderEditing.delete(delFolder="+folder+")");
}

function on_addNew()
{
	var form = document.folderEditing_Form;
	var folder = form.newFolder.value;
	GoTo("thisPage?event=folderEditing.addNew(newFolder="+folder+")");
}
