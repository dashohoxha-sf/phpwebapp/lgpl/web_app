<?
function codeViewer_onRender()
{
	global $session;

	//get the parameters of the webox
	$code_file = $session->Vars["codeViewer_codeFile"];
	$show_nr = $session->Vars["codeViewer_showLineNr"];
	$show_lines = $session->Vars["codeViewer_showLines"];
	$highlight_lines = $session->Vars["codeViewer_highlightLines"];

	codeViewer_printTable($code_file, $show_nr, $show_lines, $highlight_lines);
}

function codeViewer_printTable($code_file, $show_nr, $show_lines, $highlight_lines)
//prints the code to be displayed as an HTML table
{
	////create a code colorizer object
	//$highlighter = codeViewer_initColorizer();

	$code_lines = file($code_file);
	if ($show_lines=="")	//display all the lines
	{
		$display_ranges[] = array("start"=>1, "end"=>sizeof($code_lines) );
	}
	else
	{
		$display_ranges = codeViewer_getRanges($show_lines);
	}
	$highlight_ranges = codeViewer_getRanges($highlight_lines);

	//print the table header
	print "<table class='codeViewer_table' width='99%' cellspacing='0' "
		."cellpadding='0' border='0'>\n";

	//display all the ranges of lines
	$nr_ranges = sizeof($display_ranges);
	for ($r=1; $r <= $nr_ranges; $r++)
	{
		$range = $display_ranges[$r-1];
		$start_line = $range["start"];
		$end_line = $range["end"];
		for ($line_nr=$start_line; $line_nr<=$end_line; $line_nr++)
		{
			$line = $code_lines[$line_nr-1];
			$html_line = codeViewer_toHtml($line);

			//if the line is highlighted use other styles
			if (codeViewer_inRange($line_nr, $highlight_ranges))
			{
				$nr_class = "codeViewer_nr_highlighted";
				$line_class = "codeViewer_line_highlighted";
			}
			else
			{
				$nr_class = "codeViewer_nr";
				$line_class = "codeViewer_line";
			}

			//print the line
			print "\t<tr>\n";
			if ($show_nr)
			{
				print "\t\t<td class='$nr_class' nowrap='true'>"
					.$line_nr."&nbsp;</td>\n";
			}
			print "\t\t<td class='$line_class' nowrap='true'><pre>"
					.$html_line."</pre></td>\n";
			print "\t</tr>\n";
		}

		//print a separating line after each range
		//(except for the last one)
		if ($r < $nr_ranges)
		{
			print "\t<tr>\n";
			if ($show_nr)
			{
				print "\t\t<td class='$nr_class' nowrap='true'>"
					."--&nbsp;</td>\n";
			}
			print "\t\t<td class='$line_class' nowrap='true'>"
					."<hr /></td>\n";
			print "\t</tr>\n";
		}
	}
/*
	while (list($nr,$line) = each($code_lines))
	{
		if (codeViewer_inRange($nr+1, $highlight_ranges))
		{
			$nr_class = "codeViewer_nr_highlighted";
			$line_class = "codeViewer_line_highlighted";
		}
		else
		{
			$nr_class = "codeViewer_nr";
			$line_class = "codeViewer_line";
		}
		$html_line = codeViewer_toHtml($line);
		print "\t<tr>\n";
		if ($show_nr)
		{
			print "\t\t<td class='$nr_class' nowrap='true'>"
				.($nr+1)."&nbsp;</td>\n";
		}
		print "\t\t<td class='$line_class' nowrap='true'><pre>"
				.$html_line."</pre></td>\n";
		print "\t</tr>\n";

	}
*/
	print "</table>\n";
}

function codeViewer_toHtml($line)
{
	$line = str_replace("\t", "    ", $line);
	$line = str_replace("&", "&amp;", $line);
	$line = str_replace("<", "&lt;", $line);
	$line = str_replace(">", "&gt;", $line);
	$line = str_replace(" ", "&nbsp;", $line);

	return $line;
}

function codeViewer_getRanges($line_ranges)
//gets a string of ranges, like this: "3-7,15,20-21"
//constructs and returns a range array
{
	$ranges = explode(",", $line_ranges);
	while (list($i,$range) = each($ranges))
	{
		list($start,$end) = explode("-", $range);
		if ($end=="")	$end = $start;
		$arr_ranges[$i]["start"] = $start;
		$arr_ranges[$i]["end"] = $end;
	}

	return $arr_ranges;
}

function codeViewer_inRange($number, $arr_ranges)
//returns true if the given number is in the 
//given array of ranges
{
	while (list($i,$range) = each($arr_ranges))
	{
		$start = $range["start"];
		$end = $range["end"];
		if (($number >= $start) and ($number <= $end))
		{
			return true;
		}
	}
	return false;
}

function codeViewer_initColorizer()
//load, initialize and return the code colorizer

{
	global $BEAUT_PATH;

	$BEAUT_PATH = CODEVIEWER_PATH."codeColorizer";
	require_once "$BEAUT_PATH/Beautifier/Core.php";
	require_once "$BEAUT_PATH/HFile/HFile_mixedphp.php";
	require_once "$BEAUT_PATH/Output/Output_HTML.php";
	$highlighter = new Core(new HFile_plain(), new Output_HTML());

	return $highlighter;
}
?>
