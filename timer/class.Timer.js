
function Proc(id, comment)
//the class Proc represents the duration of a process
{
	this.ID = id;
	this.Start = new Date();
	this.End = new Date();
	this.comment = comment;
}


function Timer()
//the class Timer keeps a list of proceses (their duration)
{
	this.arrProc = new Array();
	this.nr = 0;	//nr of records

	this.find = find;
	this.Start = Start;
	this.Stop = Stop;
	this.OutputInfo = OutputInfo;
}


function find(proc_id)
//returns the position of a process in the array
//or -1 if this process cannot be found
{
	var idx;
	for (idx=0; idx < this.nr; idx++)
		if (this.arrProc[idx].ID == proc_id)
			return idx;
	//not found
	return -1;
}


function Start(proc_id, comment)
{
	var idx = this.find(proc_id);
	if (idx == -1)	//does not exist
	{
		this.arrProc[this.nr] = new Proc(proc_id, comment);
		this.nr++;
	}
	else
		alert("Timer.Start: '" + proc_id + "' already exists.");
}


function Stop(proc_id)
{
	var idx = this.find(proc_id);
	if (idx == -1)	//does not exist
	{
		alert("Timer.Start: '" + proc_id + "' does not exist.");
	}
	else
	{
		this.arrProc[idx].End = new Date();
	}
}


function OutputInfo()
{
	var output;
	output =  "<table width='100%' border='1'>\n"
			+ "\t<th align='left'> Process ID </th>\n"
			+ "\t<th align='left'> Execution Time (msec) </th>\n"
			+ "\t<th align='left'> Comment </th>\n";
	for (idx=0; idx < this.nr; idx++)
	{
		var proc = this.arrProc[idx];
		executionTime = proc.End - proc.Start;
		output += "\t<tr>\n"
				+ "\t\t<td> <b>" + proc.ID + "</b> </td>\n"
				+ "\t\t<td> " + executionTime + " </td>\n"
				+ "\t\t<td> " + proc.comment + " </td>\n"
				+ "\t</tr>\n";
	}
	output += "</table>\n";

	document.write(output);
}