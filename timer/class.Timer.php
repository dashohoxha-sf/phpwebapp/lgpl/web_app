<?
if ( !defined("class_Timer") )
//test so that it is not included multiple times
{
define("class_Timer", 1);

/*****************************************************************
/* Class TIMER is used for mesuring the execution time of
/* SQL queries or other processes or procedures.
/*****************************************************************/

class Timer
{
	var $info;		//table that will keep the time of execution

	function Timer()
	{
		$this->info = array();
	}

	function Start($processID, $comment)
	{
		if (EXECUTION_TIME_INFO==0)	return;

		if ($processID=="WebApp")	Timer::start_JS_Timer();
		$this->info[$processID]["comment"] = $comment;
		$this->info[$processID]["startTime"] = microtime();
	}

	function Stop($processID)
	{
		if (EXECUTION_TIME_INFO==0)	return;

		$this->info[$processID]["endTime"] = microtime();
	}

	function GetTime($processID)
	{
		if (EXECUTION_TIME_INFO==0)	return;

		$startTime = $this->info[$processID]["startTime"];
		list($usec1, $sec1) = explode(" ", $startTime);

		$endTime = $this->info[$processID]["endTime"];
		list($usec2, $sec2) = explode(" ", $endTime);

		$micro_sec = ($sec2 - $sec1)*1000000 + ($usec2 - $usec1);
		return $micro_sec / 1000;	//return milisecs
	}

	function OutputInfo()
	{
		if (EXECUTION_TIME_INFO==0)	return;

		Timer::output_JSTimer_info();

		print "<table width='100%' border='1'>\n";
		print "\t<th align='left'> Process ID </th>\n";
		print "\t<th align='left'> Execution Time (msec) </th>\n";
		print "\t<th align='left'> Comment </th>\n";
		while ( list($processID, $timeInfo) = each($this->info) )
		{
			$comment = $timeInfo["comment"];
			$executionTime = $this->GetTime($processID);
			print "\t<tr>\n";
			print "\t\t<td> <b>$processID</b> </td>\n";
			print "\t\t<td> $executionTime </td>\n";
			print "\t\t<td> $comment </td>\n";
			print "\t</tr>\n";
		}
		print "</table>\n";
	}

	function start_JS_Timer()
	{
		$timer_path = WebApp::to_url_path(TIMER_PATH);
		print "<script language='JavaScript' src='".$timer_path."class.Timer.js'></script>\n"
			. "<script language='JavaScript'> timer = new Timer() </script>\n"
			. "<script language='JavaScript'> timer.Start('web-page', 'time that spends the web-page to be transfered and loaded in browser') </script>\n"
			. "<!--***************-->\n";
	}

	function output_JSTimer_info()
	{
		print "\n<!--***************-->\n"
			. "<script language='JavaScript'> timer.Stop('web-page') </script>\n"
			. "<script language='JavaScript'> timer.OutputInfo() </script>\n\n";
	}

}	// End class Timer

}	// End if defined

?>