<?
if ( !defined("class_Connection") )
//test so that it is not included multiple times
{
define("class_Connection", 1);

/*****************************************************************
/* 				CLASS  CONNECTION (MySQL)
/* This class is used for opening a connection with a database
/* and for executing a query. To execute a query in the application
/* use WebApp::execQuery() which returns a Recordset.
/* In case that the application will be moved to a database other
/* then mysql, this class needs to be modified, but the application
/* will not change at all.
/*****************************************************************/

class Connection
{
	var $cnn;

	function Connection($host =DBHOST, $user =DBUSER, $passw =DBPASS, $db_name =DBNAME)
	//Opens a connection using the given parameters.
	//If no parameters are given, then uses as default values
	//the constants declared in 'config/const.DB.php'.
	{
		$this->cnn = mysql_pconnect($host, $user, $passw);
		mysql_select_db($db_name) or die("unable to select db");
	}
	
	function execQuery($query)
	//returns a mysql result
	{
		global $timer;
		static $query_id = "query_00";
		
		$query_id++;
		$timer->Start($query_id, $query);
		
		$result = mysql_query($query, $this->cnn);
		
		$timer->Stop($query_id);
	 	$this->show_error($query);
	 	
		return $result;
	}
	
	function show_error($query = "")
	{
		if (mysql_errno()==0)		return;		//no error
		if (mysql_errno()==1062)	return;		//ignore this error
		if (mysql_errno()==1136)	return;		//ignore this error
		print "Query: '$query' <br> \n";
		print mysql_errno().": ".mysql_error()." <br> \n";
	}
}	//end class Connection

}	//end if defined

?>