<?
if ( !defined("class_PageRecordset") )
//test so that it is not included multiple times
{
define("class_PageRecordset", 1);

/*****************************************************************
/* 				CLASS  PageRecordset (MySQL)
/* Same as class Recordset, but gets from the DB only a certain
/* page out of all the results that can return the query. A page
/* is a group of consecutive records, all the pages have the same
/* number of records and they form a partition of the results.
/*****************************************************************/

include DB_PATH."class.Recordset.php";

class PageRecordset extends Recordset
{
	var $recs_per_page;	
			//how many recs will be read from DB for one page
			//if 0 then all the recs will be read in one page
	var $current_page;	//which page will be read (starting from 1)
	var $nr_of_recs;	//nr of all the recs that can return the query

	function PageRecordset($id, $query ="undefined", $rp ="undefined", $cp ="undefined", $conn ="undefined")
	{
		$this->Recordset($id, $query, $conn);
		$this->recs_per_page = ($rp=="undefined" ? 0 : $rp);
		$this->current_page  = ($cp=="undefined" ? 1 : $cp);
	}
	
	function isPageRS()
	{
		return true;
	}

	function Open($cp ="default", $rp ="default", $query ="default")
	//executes the query and puts the result into $this->content
	{
		global $cnn, $session;
		
		if ($query<>"default")	$this->query = $query;
		if ($cp<>"default")	$this->current_page = $cp;
		if ($rp<>"default")	$this->recs_per_page = $rp;
		$this->nr_of_recs = $this->get_nr_of_recs();
								
		//modify and execute the query
		$first_rec_idx = ($this->current_page - 1)*$this->recs_per_page;
		$query = WebApp::replaceVars($this->query);
		$query .= " LIMIT $first_rec_idx, ".$this->recs_per_page;
		$result = $this->cnn->execQuery($query);

		$this->result2table($result);
	}

	function getPageVars()
	//returns an array of some variables that can be used in the 
	//template to indicate the next page, previous page, etc.
	{		
		$first_rec = 1 + ($this->current_page - 1)*$this->recs_per_page;
		$last_rec   = $this->current_page * $this->recs_per_page;
		if ($first_rec > $this->nr_of_recs)	{  $first_rec = $this->nr_of_recs;  }
		if ($last_rec  > $this->nr_of_recs)	{  $last_rec  = $this->nr_of_recs;  }

		$nr_of_pages = ceil($this->nr_of_recs / $this->recs_per_page);

		if ($this->current_page==$nr_of_pages)
		{ $next_page = "1"; }	//next page of the last page is the first page
		else
		{ $next_page = $this->current_page + 1; }

		if ($this->current_page==1)
		{ $prev_page = 1; }		//previous page of the first page is the first page itself
		else
		{ $prev_page = $this->current_page - 1; }

		$page_vars = array(
			"FirstRec"	=> $first_rec,			//the number of the first record of the page
			"LastRec"	=> $last_rec,			//the number of the last record of the page
			"AllRecs"	=> $this->nr_of_recs,	//the number of all records that can be retrieved by the query
			"CurrPage"	=> $this->current_page,	//current page of records that is retrieved
			"NextPage"	=> $next_page,			//the number of the next page
			"PrevPage"	=> $prev_page,			//the number of the previous page
			"LastPage"	=> $nr_of_pages			//the number of the last page
			);
		return $page_vars;
	}
	
	function get_nr_of_recs()
	//Returns the total number of records that the query
	//of the recordset can return.
	{
		global $session, $cnn;

		if ( ($this->current_page==1)	//at the first page
			or (!isset($session->Vars[$this->ID."_NR"])) )	//not in session
		{
			//find the number of recs by executing a COUNT query
			$query = WebApp::replaceVars($this->query);
			$query = $this->get_count_query($query);
			$rs = new Recordset;
			$rs->Open($query);
			$nr_recs = $rs->Field("COUNT_OF_RECS");
			$session->Vars[$this->ID."_NR"] = $nr_recs;	//save it in session
			//$webPage->print_line("<script language='javascript'> session.SetValue('".$this->ID."_NR','$nr_recs'); </script>\n");	//obsolete
		}
		else
		{
			//get it from session
			$nr_recs = $session->Vars[$this->ID."_NR"];
		}

		return $nr_recs;
	}
	
	function get_count_query($query)	//called by $this->get_nr_recs()
	//From the query of the recordset, builds and returns a COUNT query
	//which gets the number of records of the original query.
	{
		$query = eregi_replace(" from ", " FROM ", $query);
		$from_pos = strpos($query, " FROM ");
		$before_from = substr($query, 0, $from_pos);
		$after_from = substr($query, $from_pos);
		if (eregi("SELECT +DISTINCT", $before_from))
		{
			$before_from = eregi_replace("SELECT +(DISTINCT.*)", "SELECT COUNT(\\1) AS COUNT_OF_RECS", $before_from);
			$count_query = $before_from.$after_from;
		}
		else
		{
			$count_query = "SELECT COUNT(1) AS COUNT_OF_RECS" . $after_from;
		}
		
		return $count_query;
	}
}

}	//end if defined
?>