<?
if ( !defined("class_Recordset") )
//test so that it is not included multiple times
{
define("class_Recordset", 1);

/*****************************************************************
/* 				CLASS  RECORDSET (MySQL)
/* This class represents a recordset (the table returned by the
/* execution of a query). It executes the query using a given
/* connection with a database. If no connection is specified, then
/* it uses the global connection of the framework: $cnn.
/* In case of modifying the class for a DB other than MySQL, only
/* the function  result2table() needs to be changed.
/*****************************************************************/

class Recordset
{
	var $ID;			//the ID of the Recordset (usually the same as the ID of the template)
	
	var $query;			//query as string (may contain {{variables}})
	var $cnn;			//the connection that will execute the query
	
	var $content;		//the result after executing the query (2 dimensional array)
	var $pos;			//the current position in the array $content
	var $count;			//the number of rows in the array $content

	function EOF()
	{
		return $this->pos >= $this->count;
	}

	function BOF()
	{
		return $this->pos < 0;
	}

	function MoveNext()
	{
		if ( !$this->EOF() )	$this->pos++;
	}

	function MovePrev()
	{
		if ( !$this->BOF() )	$this->pos--;
	}

	function MoveFirst()
	{
		$this->pos = 0;
	}

	function MoveLast()
	{
		$this->pos = $this->count - 1;
	}

	function Field($fld_name)
	//returns the value of the field given as parameter
	{
		if ( isset($this->content[$this->pos][$fld_name]) )
			return $this->content[$this->pos][$fld_name];
		else
			return "undefined";
	}

	function Fields()
	//returns all the fields at the current position as an associated array
	{
		return $this->content[$this->pos];
	}

	function Recordset($id ="undefined", $query ="undefined", $conn ="undefined")
	//constructor
	{
		global $session, $cnn;

		$this->ID = $id;
		$this->query = $query;
		$this->cnn = ($conn=="undefined" ? $cnn : $conn);
		$this->content = array();
		$this->count = 0;
		$this->pos = 0;
	}
	
	function isPageRS()
	{
		return false;
	}

	function Open($q ="undefined")
	//executes the query and puts the result into $this->content
	{	
		if ($q<>"undefined")	$this->query = $q;
		$query = $this->query;
		$query = WebApp::replaceVars($query);
		$result = $this->cnn->execQuery($query);

		$this->Close();		//empty the content
		if ( ereg("Resource", $result) )
		{
			//the query was a 'select' and has returned some records,
			//copy the results to the table of the recordset
			$this->result2table($result);
		}
		else 
		{
			//the query was a command (insert, delete, update, etc.)
			//and it has returned TRUE or FALSE indicating success or failure
			return $result;
		}
	}
	
	function Close()
	{
		if ( isset($this->content) )	unset($this->content);
		$this->count = 0;
		$this->pos = 0;
	}

	function result2table(&$result)
	//copy the mysql $result to $this->content table
	{
		if (!$result)
		{
			print WebApp::error_msg("Query was not executed successfully.");	
			return;
		}
		
		$i = 0;
		while ( $row = mysql_fetch_array($result, MYSQL_ASSOC) )
		{
			while ( list($fld_name, $fld_value) = each($row) )
			{
				$this->content[$i][$fld_name] = $fld_value;
			}
			$i++;	//next row
		}
		mysql_free_result($result);
		$this->count = sizeof($this->content);
	}
	
	function toHtmlTable()
	//Returns the recordset as an HTML table 
	//(mostly for debuging purposes).
	{
		$type = ($this->isPageRS() ? "PageRecordset" : "Recordset");
		$htmlTable = "
<table width='90%' border='1' align='center' cellspacing='0' cellpadding='0'>
	<caption align='left'>
		<b>$type :</b> $this->ID <br>
		<b>Query :</b> $this->query 
	</caption>";
		if ($this->count > 0)
		{
			$this->MoveFirst();
			$rec = $this->Fields();
			$htmlTable .= "\t<tr>\n";
			while ( list($fld_name, $fld_value) = each($rec) )
			{
				$htmlTable .= "\t\t<th> $fld_name </th>\n";
			}
			$htmlTable .= "\t</tr>\n";
			while (!$this->EOF())
			{
				$rec = $this->Fields();
				$htmlTable .= "\t<tr>\n";
				while ( list($fld_name, $fld_value) = each($rec) )
				{
					$htmlTable .= "\t\t<td> $fld_value </td>\n";
				}
				$htmlTable .= "\t</tr>\n";
				$this->MoveNext();
			}
		}
		$htmlTable .= "</table>\n";
		
		return $htmlTable;
	}
}

}	//end if defined
?>