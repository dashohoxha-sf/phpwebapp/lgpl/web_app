<?

/*******************************************************************
/*	       		CLASS WEB APPLICATION
/* Contains some functions that are used frequently in the application.
/* These functions could have been defined in the global scope as well,
/* but instead they are defined inside a class just for the sake of
/* the namespace. They are called like this: WebApp::func_name() which
/* makes clear that they are functions of the web application framework.
/*******************************************************************/

class WebApp
{
	function callFreeEvent(&$event)
	{
		$file_name = "on.".$event->name.".php";
		$fun_name = "on_".$event->name;

		//look for the event handler in the path of targetPage
		$path = TPL_PATH.dirname($event->targetPage);
		$fname = $path."/".$file_name;
		if (file_exists($fname))
		{
			include $fname;
			$fun_name(&$event);
			return;
		}

		//look for the event handler in the path of sourcePage
		$path = TPL_PATH.dirname($event->sourcePage);
		$fname = $path."/".$file_name;
		if (file_exists($fname))
		{
			include $fname;
			$fun_name(&$event);
			return;
		}

		//look for the event handler in the common path
		$fname = EVENTHANDLER_PATH.$file_name;
		if (file_exists($fname))
		{
			include $fname;
			$fun_name(&$event);
			return;
		}

		//no event handler found
		print WebApp::error_msg("event handler '$file_name' not found.");
	}
	
	/******** begin construct page functions **********
	/* WebApp::constructHtmlPage() constructs an HTML page.
	/* By default the page is sent directly to the browser.
	/* When you don't want to sent the page to the browser, 
	/* then call WebApp::collectHtmlPage() before constructing it.
	/* Then you can use WebApp::getHtmlPage() to get the
	/* constructed page as a string.
	/*/
	function constructHtmlPage($tpl_file)
	{
		global $session, $webPage, $timer;

		//call the handler 'on_beforePage' if it exists		
		$fname = EVENTHANDLER_PATH."on.beforePage.php";
		if (file_exists($fname))
		{
			include_once $fname;
			on_beforePage($event);
		}

		//load the configuration of the page 
		//by parsing the template file
		$webPage->load($tpl_file);
				
		//generate the html page
		$webPage->rootTpl->toHtml();
				
		//call the handler 'on_afterPage' if it exists		
		$fname = EVENTHANDLER_PATH."on.afterPage.php";
		if (file_exists($fname))
		{
			include_once $fname;
			on_afterPage($event);
		}
	}
	
	function collectHtmlPage()
	{
		global $webPage;
		return $webPage->collect = true;
	}
	
	function getHtmlPage()
	{
		global $webPage;
		$html_page = $webPage->html_page;
		$webPage = new WebPage;
		return $html_page;
	}
	/******** end construct functions **********/
	
	/******** begin variable functions **********/
	function addVar($var_name, $var_value)
	{
		global $tplVars;
		$tplVars->addVar($var_name, $var_value);
	}
	
	function addGlobalVar($var_name, $var_value)
	{
		global $tplVars;
		$tplVars->addGlobalVar($var_name, $var_value);
	}
	
	function addVars($arrVars)
	//declares a list of variables at once
	{
		global $tplVars;
		$tplVars->addVars($arrVars);
	}
	
	function addGlobalVars($arrVars)
	//declares a list of variables at once
	{
		global $tplVars;
		$tplVars->addGlobalVars($arrVars);
	}
	
	function getVar($var_name)
	//returns the value of the variable, or "undefined" if not found
	{
		global $tplVars;
		return $tplVars->getVar($var_name, $var_value);
	}
	/******** end variable functions **********/

	/******** begin evaluate **************/
	function evaluate($expr)
	//returns the value of the expression $expr
	{
		//declare as global any variables that are used in $expr
		$var_names = implode(",", WebApp::get_var_names($expr));
		if ($var_names <> '')
		{
			eval("global $var_names;");
		}
		eval("\$webApp_value = $expr;");
		return $webApp_value;
	}
	function get_var_names($expr)
	//used by evaluate; returns an array with all the variable
	//names that are used in $expr (which will be declared as global)
	{
		//this trick is done because ereg
		//does not handle well the character '$'
		$expr = str_replace("$", "###", $expr);

		$var_names = array();
		while ( ereg("###([[:alnum:]_]*)", $expr, $regs) )
		{
			$var_names[] = "\$".$regs[1];
			$expr = str_replace($regs[0], "", $expr);
		}
		return $var_names;
	}
	/******** end evaluate **************/
	
	
	function getVarValue($var_name)
	//returns the value of a variable by looking first at
	//$tplVars, and then at $session->Vars,
	//then at PHP constants, and finally at PHP global variables;
	//if this variable not found, returns the var_name itself
	{
		global $session;

		//search first in the template variables ($tplVars)
		$var_value = WebApp::getVar($var_name);
		if ($var_value<>"undefined")	//found
		{
			return $var_value;	
		}

		//if not found look at session variables
		if ( isset($session->Vars[$var_name]) )
		{
			$var_value = $session->Vars[$var_name];
			return $var_value;
		}

		//if not found look at PHP constants
		if ( defined($var_name) )
		{
			eval("\$tmp = $var_name;");
			$var_value = $tmp;
			return $var_value;
		}

		//if not found look at PHP global variables
		if ( isset($GLOBALS[$var_name]) )
		{
			$var_value = $GLOBALS[$var_name];
			return $var_value;
		}

		//if still not found return the variable name itself
		$var_value = $var_name;
		return $var_value;
	}
	
	
	/******** begin  replaceVars() **************
	/* Search in the given line for variable occourences
	/* like this: {{var_name}} and replace them by their values.
	/*
	/* If single (double) quotes surround the var_name, 
	/* like this: {{'var_name'}} ({{"var_name"}}), then all occourences 
	/* of the single (double) quote inside the value of the variable 
	/* are escaped, like this: \' (\"). This is done because sometimes
	/* a variable is inside a string, and if it contains quotes inside
	/* its value, then it may break the string.
	/*/
	function replaceVars($line)
	{
		while ( ereg("{{([^}]*)}}", $line, $regs) )
		{
			$str2replace = $regs[0];
			$var_name = $regs[1];
			$var_value = $regs[1];
			list($var_name, $quote) = WebApp::get_quote($var_name);
			$var_value = WebApp::getVarValue($var_name);
			$var_value = WebApp::escape_quotes($var_value, $quote);
			$line = str_replace($str2replace, $var_value, $line);
		}
		return $line;
	}
	function get_quote($var_name)
	//gets the quote which surrounds the $var_name
	//single, double, or none
	{
		if ( ereg("^'(.*)'$", $var_name, $regs) )
		{	//surrounded by single quotes
			$var_name = $regs[1];
			$quote = "'";
		}
		elseif ( ereg('^"(.*)"$', $var_name, $regs) )
		{	//surrounded by double quotes
			$var_name = $regs[1];
			$quote = '"';
		}
		else
		{	//not surrounded by quotes
			$quote = '';
		}
		return array($var_name, $quote);
	}
	function escape_quotes($var_value, $quote)
	//escapes all the quotes in the $var_value
	{
		switch ($quote)
		{
			case '"':		//double quote
				$var_value = str_replace('"', '\"', $var_value);
				$var_value = '"'.$var_value.'"';
				break;
			case "'":		//single quote
				$var_value = str_replace("'", "\'", $var_value);
				$var_value = "'".$var_value."'";
				break;
			case '':		//no quotes
				//do nothing
				break;
			default:
				print "warning: escape_quotes: the quote '$quote' is unknown.<br>\n";
				break;
		}
		return $var_value;
	}
	/******** end replaceVars() **************/

	/******** begin tag processing functions **********/
	function get_tag_name($line)
	//if the $line starts with an opening or closing tag
	//('<tag_name' or '</tag_name') then it returns it
	//otherwise returns 'undefined'
	{
		$line = trim($line);
		if ( ereg("^(</?[^[:space:] />]*)", $line, $regs) )
		{
			return $regs[1];
		}
		else
		{
			return "undefined";
		}
	}
	
	function get_attr_value($line, $attrib)
	//returns the value of the given attribute from the given
	//line (assuming that the line is a start element), or
	//"undefined" if not found
	{
		$line = trim($line);
		$pattern = ' '.$attrib.'="([^"]*)"';
		if ( ereg($pattern, $line, $regs) )
		{
			return $regs[1];
		}
		else
		{
			return "undefined";
		}
	}
	/******** end tag processing functions **********/
	
	/*********** begin DB functions *************/
	function execQuery($query, $conn ="undefined")
	//executes the query and returns a Recordset
	{
		$rs = new Recordset("undefined", "undefined", $conn);
		$rs->Open($query);
		return $rs;
	}
	
	function execDBCmd($cmd_id)
	//executes the DB command with the given id
	//returns TRUE or FALSE indicating success or failure
	{
		global $webPage;
		$rs = $webPage->getRecordset($cmd_id);
		return $rs->Open();
	}
	
	function openRS($rs_id)
	//opens and returns the recordset with the given id
	{
		global $webPage;
		$rs = $webPage->getRecordset($rs_id);
		$rs->Open();
		return $rs;
	}
	/*********** end DB functions *************/
	
	/******** mix functions **********/
	function to_url_path($local_path)
	//takes a local path and returns it as an url location
	{
		//sometimes APP_PATH may be empty '', and str_replace()
		//does not accept an empty argument; this is why the
		//trick of "#" is used
		$url_path = str_replace("#".APP_PATH, APP_URL, "#".$local_path);
		return $url_path;
	}
	
	function error_msg($err_msg)
	//formats and returns an error message 
	{
		return "\n<div class='error_msg'>Error: ".$err_msg."</div>\n";
	}
		
}	//End of class WebApp

?>
