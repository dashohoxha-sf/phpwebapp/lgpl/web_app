<?
//paths of the application framework
include WEBAPP_PATH."const.Paths.php";

//include timing functions
include TIMER_PATH."class.Timer.php";

//include session
include SESSION_PATH."class.Event.php";
include SESSION_PATH."class.Session.php";

//include page constructor component
include CONSTRUCTOR_PATH."class.Template.php";
include CONSTRUCTOR_PATH."class.MainTpl.php";
include CONSTRUCTOR_PATH."class.IfTpl.php";
include CONSTRUCTOR_PATH."class.RepeatTpl.php";
include CONSTRUCTOR_PATH."class.WebBox.php";
include CONSTRUCTOR_PATH."class.WebPage.php";
include CONSTRUCTOR_PATH."class.VarStack.php";

include WEBAPP_PATH."class.WebApp.php";

//declare some global variables 
$timer		= new Timer;
$event		= new Event("undefined.undefined");
$session	= new Session;		//it also declares a global $event object which overrides the above one;	
$webPage	= new WebPage;
$tplVars	= new VarStack;		//keeps the template variables


if (USES_DB)
{
	//include database component
	include DB_PATH."class.Connection.php";		
	include DB_PATH."class.Recordset.php";
	include DB_PATH."class.PageRecordset.php";
	
	//Create a default connection for the application. Parameters 
	//of this connection are specified in 'config/const.DB.php'.
	//This connection will be used for each interaction with a DB,
	//if no other connection is specified.
	$cnn = new Connection;	
}

define("WEBAPP_URL", WebApp::to_url_path(WEBAPP_PATH));

?>
