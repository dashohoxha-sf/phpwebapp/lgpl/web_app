<?
function panel_onRender()
{
	global $session;
	
	$codeView = $session->Vars["codeView"];
	if ($codeView=="true")
	{
		WebApp::addVars( array(
			"codeView_checked" => 'checked="true"',
			"preview_checked"  => ''
		));
	}
	else
	{
		WebApp::addVars( array(
			"codeView_checked" => '',
			"preview_checked"  => 'checked="true"'
		));
	}
}
?>
