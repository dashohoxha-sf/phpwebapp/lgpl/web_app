<?
function fileBrowser_eventHandler($event)
{
	global $session;

	switch ($event->name)
	{
		case "file":
			$root = $session->Vars["folderListing.root"];
			$path = $session->Vars["folderListing.currentPath"];
			$tpl_file = $event->args["file_name"];
			$session->Vars["fileView_file"] = $root.$path.$tpl_file;
			break;
	}
}
?>
