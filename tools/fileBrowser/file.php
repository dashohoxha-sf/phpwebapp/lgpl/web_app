<?
function file_onLoad()
{
	global $session;
	if ( !isset($session->Vars["fileView_file"]) )
	{
		$current_path = WebApp::getVar("./");
		$session->Vars["fileView_file"] = $current_path."sample.html";
	}
}

function file_onRender()
{
	global $session;
	
	$tpl_file = $session->Vars["fileView_file"];
	$path = dirname($tpl_file);
	$fname = basename($tpl_file);
	if (!ereg("\.html?$", $fname))
	{
		//not a template file
		return;
	}

	//get the config file name
	$fname = ereg_replace("\.html?$", ".cfg.php", $fname);
	$cfg_file = $path."/".$fname;

	//load the config file (if it exists)
	if (file_exists($cfg_file))
	{
		include $cfg_file;
		$func_name = ereg_replace("\.html?$", "", basename($tpl_file));
		$func_name .= "_config";
		$func_name();	//call the config function
	}
}
?>
