
function on_refresh()
{
	var form = document.changeViewForm;
	var hide = form.hide;
	var preview = form.viewMode[1];
	var visible = !hide.checked;
	var codeView = !preview.checked;
	GoTo("?sessChange=folderVisible="+visible+":codeView="+codeView);
}

function on_changeSess()
{
	var form = document.changeViewForm;
	var var_name = form.var_name.value;
	var var_value = form.var_value.value;
	GoTo("?sessChange="+var_name+"="+var_value);
}

function on_changePath()
{
	var form = document.changeViewForm;
	var root_folder = form.root_folder.value;
	GoTo("?event=folderListing.changeRoot(root_folder="+root_folder+")");
}
