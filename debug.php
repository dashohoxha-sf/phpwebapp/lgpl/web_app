<?
function debug_onRender()
{
	global $session, $webPage;
	
	$debug_goto = (DEBUG_GOTO ? "<script language='javascript'> debug_GoTo = true; </script>" : "");
	WebApp::addVar("DEBUG_GOTO", $debug_goto);
	
	$debug_session = (DEBUG_SESSION ? $session->to_HTML_table() : "");
	WebApp::addVar("DEBUG_SESSION", $debug_session);

	$debug_recordsets = (DEBUG_RECORDSETS ? $webPage->recordsets_to_html() : "");
	WebApp::addVar("DEBUG_RECORDSETS", $debug_recordsets);
	
	$debug_templates = (DEBUG_TEMPLATES ? $webPage->tpl_to_tree() : "");
	WebApp::addVar("DEBUG_TEMPLATES", $debug_templates);
}
?>