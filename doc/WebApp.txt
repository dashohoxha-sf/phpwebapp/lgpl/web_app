
What is phpWebApp
-----------------
phpWebApp is an application framework which makes easy and simple
the task of building PHP web applications based on relational
databases. It separates the task of designing and changing the
layout of the application from the task of implementing the logic
of the application, by using XML templates that are an extension
of XHTML. It also simplifies the task of implementing the logic of
the application by using an event based programming model.
In addition, phpWebApp tries to offer code reusability to the community
of webApp developers, by means of components and boxes (web controls).


The phpWebApp templates
------------------------
These are normal XHTML files which have some additional tags and
some variables. These tags and variables are processed by the
framework and are replaced with XHTML code when the web page is rendered.
These templates allow the web page designer to work independently
from the database designer and the programer.

For more details read the file: xhtml_template_specification.txt .


The phpWebApp event based programming model
--------------------------------------------
A PHP Web Application built on top of phpWebApp is an event-driven
message-based application. Events are the clicking of links or
buttons in browser. When such an event happens, a message is sent
to the application framework using a JavaScript function, like this:

	GoTo('page=page_name&do=some_action&param1=val1&param2=val2')

'page_name' is the name of the page that will be rendered by the
phpWebApp after processing this message, 'some_action' tells to
phpWebApp what to do before rendering the page (which event-handler
to call for handling this event), and the  'param1', 'param2' etc.
are parameters which are used by this event handler.

(?
	Maybe it is better: raise_event('event1'), which triggers
	the JS function handle_event1(), which makes some error
	checking and then calls GoTo('...').
?)
(?
	Probably it could be formalized like this (in the context
	of templates):
	<Event name="event1" target="page_name"> Button1 </Event>
?)

If you have ever used VB, then it may help to make an analogy with VB.
Several pages of a phpWebApp application are like several forms of
a VB application, with the difference that the web application
cannot open several pages at the same time, but only one at a time
(in browser). The events of a webApp are like events of a VBApp.
The webApp events:
  - do something on the client-side (error-checking etc) (optionally)
  - then make a request to the server for another page
	(or refresh the same page) (optionally)
  - and in this request they raise a server-side event (optionally)


The folder of a web page
-------------------------
In order to render a page, phpWebApp needs a template and an optional
configuration file. The phpWebApp looks for these in a folder
named after the name of the page (under the root folder of the
application).  E.g. if we tell phpWebApp to render the page named
'page1', then it looks for the template file 'page1/page1.html'
and for the config file 'page1/page1.tpc'. In general, for every
file that is needed in order to render this page, phpWebApp looks
at this folder or at its subfolders (e.g. to include 'page1.js'
the phpWebApp looks for 'page1/page1.js')
(?
	to include 'image1.gif' it first looks for 'page1/img/image1.gif',
	and if it does not find it there it looks at 'page1/image1.gif';
	and if it still doesn't find it, it looks at 'images/image1.gif
	(where 'images/' is a predefined folder that keeps the common
	images of the application)
?)

So, for every web page in the application, there is a separate
folder that contains all the files and resources related to this
web page.


Other folders of an application
--------------------------------
A special folder that each application should have is the folder
'config'. At this folder are placed several configuration files.
such as 'const.DB.php' (which tells to phpWebApp where the data
of the application are stored), 'const.Settings.php' (which contains
some constants that customize the behavior of the application),
'init.Session.php' (which tells to phpWebApp how to initialize the
session the first time that the user enters in the application), etc.

The files and resources that are common for several pages are placed
at common folders of the application. E.g. if there are some
php files that are included in several event_handlers (belonging to
different pages) then create a folder 'include_php' and put them
there. If there are some images or JS functions that are used in
several pages, then create a folder 'images' or 'js_scripts' and put
them there, and so on. These folders are optional and are created
at the will of the developer for better organizing the files.

In order to make the usage of these folders flexible, use php
constants to keep the path of these folders. E.g., suppose that we
have created a folder 'include_php' which contains the file
'file1.php' and we want to include this inside 'file2.php'. Then
we should add this line to 'config/const.Paths.php':
	define("INCLUDE_PATH",	APP_PATH."include_php/");
and use this line inside 'file2.php' for including 'file1.php':
	include INCLUDE_PATH.'file1.php';
The benefit of this is that, if we later decide to change the name
of the folder from 'include_php' to 'inc', or if we decide to place
it under another folder (for better organizing the files), then all
we have to change is only 'const.Paths.php', and everything else
remains unchanged.

(?
	Probably it would be better if phpWebApp *enforced* the flexible
	usage of the folders, rather then letting it at the will of the
	developer. Maybe it could be enforced if the template processor
	didn't allow absolute paths inside the templates, like this:
	<img src="/images/image1.gif">, and required the usage of variables
	like this: <img src="{{IMG_URL}}image1.gif">, which would force
	the developer to declare the constant IMG_URL in 'const.Paths.php'.
?)


Server-side event handlers
---------------------------
How does phpWebApp find which server-side event handler to call
for a certain event 'event1'?
  - It first looks for the file 'target_page/do.event1.php' and
	includes it (target_page is the page that will be rendered after
	calling the event-handler).
  - If it does not find it there, then it looks for
	'source_page/do.event1.php' and includes it (source_page is the page
	from which the event comes).
  - If it does not find it there, then it looks for
	EVENT_HANDLER_PATH.'do.event1.php' and includes it (EVENT_HANDLER_PATH
	is a predefined constant which keeps the path of common event handlers,
	it has a default value of 'event_handlers/', but the user can change
	it in 'const.Paths.php').
  - If it doesn't find it there, then phpWebApp gives an error message
	or a warning.

(?
	Probably could be better to name the event handler 'on.event1.php'
	or something else, instead of 'do.event1.php'.
?)


Predefined events
------------------
Beside the user defined events, which happen when the user clicks
something in the browser, the phpWebApp raises some other (predefined)
events. E.g. when a user defined event causes a transition from 'page1'
to 'page2', then the exit event of the 'page1' and the 'enter' event
of the 'page2' are triggered. The phpWebApp looks for 'page1/on.exit.php'
and for 'page2/on.enter.php' event-handlers (defined by the user), and if
it finds them, calls them. Other predefined events are 'before_event'
and 'after_event' which are triggered automatically before and after calling
a user defined event-handler ('on.before_event.php', 'on.after_event.php').
Another predefined event is 'init_app'('on.init_app.php') which is triggered
when the user for the first time opens the application in browser (corresponds
to the beginning of a session).

(?
	What other predefined events can there be? That is which other events
	are needed so that phpWebApp is general enough and is not unnecessarily
	overloaded?
?)

When a predefined event is triggered, phpWebApp looks for an event-handler
for this event. If the user has not defined any event-handler for this
event, then different from user defined events, nothing happens (no error
or warning message is displayed).

As an example, suppose that you want to keep track of what features of the
application the web user uses most. Then you can write a handler for the
predefined event 'before_event' or 'after_event' (by creating the file
'on.before_event.php' or 'on.after_event.php') and there you can record
which user event was raised by the web user, etc. From these records you
can then later derive statistics. The point is that 'on.before_event.php'
is called before each user defined event is handled.


How does a webApp makes use of the phpWebApp framework
-------------------------------------------------------
A web application uses the phpWebApp by including it at 'index.php' which
is placed at the root of the application. This is the main and only php
file of the application and it is almost the same for each application.
The other php files are either event-handlers, which are called by the
framework at the appropriate times, or include files which are included
inside these event-handlers. 'index.php' is a small file:

	<?
	//get the application path
	$app_path = dirname(__FILE__);
	define("APP_PATH",		$app_path."/");

	//load the WebApp framework to do the rest of the work
	$webapp_path = "/home/dasho/public_html/WebApp/";
	include $webapp_path."WebApp.php";
	?>

The only thing that may change here is the $webapp_path, which is the
path of phpWebApp.


The component structure of phpWebApp
-------------------------------------
The phpWebApp framework is built by components which are independent of
each-other, almost self-contained, and have a minimal interface with
each-other and with the application. This encapsulation and independence
allows these components to be changed, improved or even totally rewritten,
without changing the rest of phpWebApp or the applications based on it.
The phpWebApp architecture also allows for other components to become part
of it later. Eventually it may come up to that point that for everything
that one usually needs in a web application he may find it ready in the
phpWebApp framework.

The components are divided into these categories:
1 - Main (or Core) components
2 - Additional (optional) components
3 - Utilities
4 - Boxes (WebControls)


Main (Core) components of phpWebApp
------------------------------------
These are essential components that are usually used by every webApp,
so they are always used by the framework. These are:

  - Template processor
  - Session support
  - DB interface

(?
	Probably Session and DB could become optional, because there may
	be some very simple applications that don't need them? Or, if this
	is the case, then these applications don't have to use phpWebApp.
?)


Additional (optional) components of phpWebApp
----------------------------------------------
These are components that are needed in some applications and are not
needed in some others. So the application developer can enable or
disable them at will (by default they are disabled, and the developer
enables those that the application needs).

  - Dynamic Hierarchical Menus
	Enables an application to easily incorporate into a page hierMenus.
  - Popup MsgBox
	Enables an application to easily display a popup message box from
	the server-side script to the user (e.g. for displaying an error
	message or a notification).
  - tool_tip_text
  - advertisement
  - multiple language support


Utility components of phpWebApp
--------------------------------
  - performance measurement
	Enables the developers to measure the execution times of several
	processes (e.g. when they want to find the bottlenecks in performance).
  - usage tracking (statistics)
  - client sniffer

 (?
 	What is the difference between the optional components and the
 	utilities?
 ?)


What are Boxes (or WebControls)
--------------------------------
(in analogy with VB controls)
A Web Control is a part of web page which has an appearance and functionality
that may be needed in several pages. The appearance, client-side behavior
and server-side behavior of a web control is well encapsulated and it is
loosely coupled with the other parts of the application. This allows a
developer to easily incorporate a web control in several pages of the
application (or in different applications, if applicable).

A web control may be composed of other web controls, and a web control may
represent an entire page (all the page is composed of a single web control).

Examples of web controls: a login box, a search box, a calendar, a basket, etc

Example usage:
	<Box id="Search"/>
or:
	<Box id="Search">
		<param name="param1" value="val1"/>
		<param name="param2" value="val2"/>
	</Box>
Another example:
	<ArticleList rsID="recordset1">
		<param name="hasGotoLast" value="false"/>
	</ArticleList>

Each WebControl files and resources are saved in the same folder (which
has the name of the control). The files needed for a WebControl are:
  - a template that designs the display of the control
  - a configuration file (optional)
  - a JS file that keeps the client side logic of the WebControl
  - php event-handlers of the server-side events
  - etc


Predefined Boxes (WebControls)
-------------------------------
These are WebControls that are quite general to be used in different
applications.
The predefined web controls can be tailored to the needs of the current
application by a process similar to the derivation of classes (e.g. web
control B extends web control A and then it overrides some of the appearance,
client-side behavior, or server-side behavior of A).

Some of the predefined web controls may be:
  - Login
  - Search
  - Basket
  - E-mail
  - etc


User defined Boxes (WebControls)
---------------------------------
These are WebControls that are specific for the application and can be used
in several pages of the application. E.g. if there is a list of items that
needs to be displayed in several pages, then, in order to simplify the
task of building and maintaining these pages, the developer may decide to
make it a web control and to include it in all the pages that need it. When
it comes to make a change or an improvement to the item list, then this change
needs to be done in only one place, in the web control, instead of in many
places. Or the developer may decide to use web controls just for modularity,
in order to split a big page in several smaller web controls.

Some examples of user defined web controls may be:
  - ArticleList
  - ArticleDetails
  - Services
  - etc

(?
	Is it possible to unify the 'components', 'utilities' and the 'boxes',
	so that they are not different categories but are the same category?
?)


Web Application Tools and Wizards
----------------------------------
Along with the web application framework, useful for quickly building
web applications would also be some tools and wizards.

For example, a useful tool could be a converter from a template to a sample
xhtml page, and then back from the xhtml page to the template. This would
allow the page designer to work completely independent of the part of the
application which makes it dynamic, without having to worry about the extra
tags and the variables of the templates.

Also, an online wizard for quickly building a new phpWebApp application and
for configuring, customizing and modifying an existing phpWebApp application
could be helpful. This wizard can be built using the phpWebApp framework
itself.


-----------------
(?
	Could phpWebApp be generalized so much that it becomes a web
	application framework not only for PHP, but also for JSP etc.?
?)

(?
	When the request for a web page comes, PHP reads and parses a lot of
	classes and includes of phpWebApp just to generate and output a single
	page. The same happens for the next request, and in general, for every
	event that user triggers on the page. It would be more efficient if
	phpWebApp was a web application server (i.e. all the main classes and
	includes of the framework (that are used by almost every page) are loaded
	in memory once and stay there as a service. When a new page request
	comes they are already loaded in memory and this may result in better
	performance. Can it be done?
?)
