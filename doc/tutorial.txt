

1 - Sample 1: Transitions
	A very simple application that has 2 or 3 HTML files
	which have transitions to each-other and have some
	links to external pages. 
	Explains the folder structure of a simple application,
	explains what are the transitions and the difference
	between the transitions and the links. Explains how to
	debug transitions.
	
2 - Sample 2: <Include> and <!--# Comments #-->
	Based on Sample_1 but adds some additional things.
	The HTML files now become templates. This application 
	uses the <Include> tag to insert the same header file 
	at the beginning of each template and to break the 
	templates into sub templates. It shows how to include
	the javascript code and the stylesheet of each template.
	It also explains the use of the framework comments and 
	the difference between them and the HTML comments.
	Explains how to debug the template structure.
	
3 - Sample 3: Introducing WebBox-es
	Develops further the Sample_2. Some of the templates are
	turned into weboxes. Explains the fundamental difference
	between a template and a webox. The PHP code of the weboxes
	declares the function onRender(), where some variables
	are added using WebApp::addVar(), and these {{variables}}
	are used in the corresponding template.
	
4 - Sample 4: Sending and Handelling Events
	Develops further the Sample_3. Some of the transitions send
	events to some of the weboxes. The target weboxes handle
	these events in their eventHandler() functions. Explains also
	the transitions to 'thisPage', sending an event to 'any' webox,
	and sending events to 'none' of the weboxes (independent events).
	
5 - Sample 5: Session and Variables
	Explains what are the session variables and how to use them.
	Explains further how the {{template variables}} are evaluated.
	Explains how the session variables are used to keep the state
	of the weboxes and the states of the application (introduces
	the function onLoad() of the weboxes). Explains how the <If>
	elements are used to display templates conditionally.
	Explains how to debug the session variables.

6 - Sample 6: Interacting with a Database
	Explains how to set a default connection with a database.
	Explains the configuration file of a template (*.tpc) and
	the <Recordset> element. Explains the <Repeat> elements and
	how to use them inside a template. Explains the '.db' file
	of a webox, the <Recordset> and <dbCommand> elements inside it,
	and how to use the functions WebApp::execDBCmd(), WebApp::openRS()
	and WebApp::execQuery() inside the PHP code of the webox.
	Explains how to debug the recordsets of the page and the execution
	times of the queries (and other processes).

7 - Sample 7: All the rest of the advanced features
	A complicated, real and almost finished web application (which
	may evolve from the previous samples or may be independent of
	them), which makes use and explains almost all of the advanced
	features of framework which have not been explained in the 
	previous samples. Such features may be:
	- <Var> elements
	- Detailed explanation of the <Repeat> element and the page
	  recordsets.
	- <Repeat> inside another <Repeat> that uses in the recordset
	  a field from the first <Repeat>
	- An <Include> element that uses a variable.
	- Usage of an external webox.
	- Collecting a page (instead of sending it to the browser)
	- Etc.
