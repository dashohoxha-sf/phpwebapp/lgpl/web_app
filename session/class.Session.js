
//Var class represents a single variable
function Var(name, value)
{
	this.Name	= name;
	this.Value	= value;
}

//SessionVars class keeps a list of variables.
//These variables can be read, changed, etc.
function SessionVars()
{
	this.Vars	= new Array();
	this.nr		= 0;		//the number of variables

	this.find		= find;
	this.AddVar		= AddVar;
	this.GetValue	= GetValue;
	this.SetValue	= SetValue;
	this.toStr		= toStr;
}

function find(var_name)
//Returns the index of a variable in the array
//or -1 if there is no such variable in array.
{
	var idx;
	for (idx=0; idx < this.nr; idx++)
		if (this.Vars[idx].Name == var_name)
			return idx;	//found
	//not found
	return -1;
}

function AddVar(var_name, var_value)
{
	var idx = this.find(var_name);
	if (idx == -1)	//doesn't exist
	{
		this.Vars[this.nr] = new Var(var_name, var_value);
		this.nr++;
	}
	else
		alert("SessionVars.AddVar: There is a '" + var_name + "'already in the list, use SetValue to change its value.");
}

function GetValue(var_name)
{
	var idx = this.find(var_name);
	if (idx == -1)	//not found
	{
		alert("SessionVars.GetValue: '" + var_name + "' doesn't exist.");
		return "undefined variable";
	}
	else
		return this.Vars[idx].Value;
}

function SetValue(var_name, var_value)
{
	var idx = this.find(var_name);
	if (idx == -1)	//not found
		alert("SessionVars.SetValue: '" + var_name + "' doesn't exist.");
	else
		this.Vars[idx].Value = var_value;
}

function toStr()
//Returns a string of all variables and their values concatenated.
{
	var strVars = "";
	var i;
	for (i=0; i < this.nr; i++)
		strVars += this.Vars[i].Name+"="+this.Vars[i].Value+"&";
	return strVars;
}
