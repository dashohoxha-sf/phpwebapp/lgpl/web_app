<?
if ( !defined("class_Session") )
//test so that it is not included multiple times
{
define("class_Session", 1);

/***************************************************************/
/* Class SESSION is used to keep session variables
/***************************************************************/

class Session
{
	var $Vars;	//associated array that keeps the session vars

	function Session()
	{
		global $sessionVars;

		if ( isset($sessionVars) )
		{
			$this->Load();
		}
		else
		{
			$this->Init();
		}

		$this->extractPhpVars();
	}
	
	function Init($sessID ="empty")
	{
		$this->setNewID($sessID);
		$fname = CONFIG_PATH."init.Session.php";
		if (file_exists($fname))	
		{
			include $fname;
			while (list($var_name,$var_value) = each($arrSession))
			{
				$this->AddVar($var_name, $var_value);
			}
		}
	}

	function setNewID($newID ="empty")
	{
		if ($newID=="empty")
		{
			global $REMOTE_ADDR;
			$this->Vars["ID_S"] = "IP: $REMOTE_ADDR; DATE: ".date("Y/m/d/ H:i:s");
		}
		else
		{
			$this->Vars["ID_S"] = $newID;
		}
	}

	function str2arr($strVars)
	//Helper function that explodes the string given as parameter
	//and returns an associated array of var_name and var_value.
	//The format of $strVars is:  var1=val1&var2=val2&...
	{
		$arrVars = array();
		$arr = explode("&", $strVars);
		while (list($i,$strVar) = each($arr))
		{
			if ($strVar=="")	continue;
			list($var_name,$var_value) = split("=", $strVar, 2);
			$arrVars[$var_name] = $var_value;
		}
		return $arrVars;
	}

	function Load()
	//Explodes the global variable $sessionVars
	//and fills the array $this->Vars with the session variables.
	//The format of $sessionVars is:  var1=val1&var2=val2&...
	{
		global $sessionVars;

		$arrVars = $this->str2arr($sessionVars);
		while (list($var_name,$var_value) = each($arrVars))
		{
			$this->AddVar($var_name, $var_value);
		}
	}

	function extractPhpVars()
	//Explodes $phpVars and declares some PHP global vars.
	//The format of $phpVars is:  var1=val1&var2=val2&...
	{
		global $phpVars;

		$arrVars = $this->str2arr($phpVars);
		while (list($var_name,$var_value) = each($arrVars))
		{
			if ($var_name=="sessChange")
			{
				//change the session according to this string
				$this->Change($var_value);
			}
			else if ($var_name=="event")
			{
				//create an event object
				global $event;
				$event = new Event($var_value);
			}
			else	
			{
				//global PHP variable
				global $$var_name;
				$$var_name = $var_value;
			}
		}
	}

	function Change($strChange)
	//Sets new values to some session vars,
	//according to the parameter $strChange.
	//The format of $strChange is:  var1=val1:var2=val2:...
	{
		if ($strChange == "") { return; }

		$arrSession = explode(":", $strChange);
		while ( list($i, $strVar) = each ($arrSession) )
		{
			list($var_name, $var_value) = explode("=", $strVar);
			$this->Vars[$var_name] = $var_value;
		}
	}

	function AddVar($var_name, $var_value)
	//Adds a new variable in the session.
	{
		$this->Vars[$var_name]	= $var_value;
	}

	function UnsetVar($var_name)
	//Removes a variable from the session.
	{
		unset($this->Vars[$var_name]);
	}

	function to_JS()
	//Builds and returns the variable $session_vars which is used to insert
	//the session vars inside the page as JavaScript code 
	{
		$session_vars = "<script language='JavaScript'>\n";
		$session_vars .= "\tsession = new SessionVars();\n";
		reset($this->Vars);
		//add each variable of the session
		while (list($var_name,$var_value) = each($this->Vars))
		{
			$session_vars .= "\tsession.AddVar('$var_name','$var_value');\n";
		}
		$session_vars .= "</script>";

		return $session_vars;
	}

	function to_HTML_table()
	//Builds an HTML table containing the session vars
	//and returns it as a string (for debug).
	{
		$session_html = "<hr />\n"
			. "<table border='1'>\n"
			. "\t<caption align='left'> <strong>Session Variables:</strong> </caption>\n"
			. "\t<th>var_name</th>\n"
			. "\t<th>var_value</th>\n";
		reset($this->Vars);
		while (list($var_name,$var_value) = each($this->Vars))
		{
			$session_html .= "\t<tr>\n"
				. "\t\t<td>$var_name</td>\n"
				. "\t\t<td>$var_value</td>\n"
				. "\t</tr>\n";
		}
		$session_html .= "</table>\n<hr />\n";

		return $session_html;
	}

}	// End class Session

}	// End if defined
?>