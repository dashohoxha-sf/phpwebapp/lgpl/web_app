<?
if ( !defined("class_Event") )
//test so that it is not included multiple times
{
define("class_Event", 1);

/***************************************************************/
/* Class EVENT is used to keep the data of an event
/***************************************************************/

class Event
{
	var $sourcePage;	//the page that sent the event
	var $targetPage;	//the page that should receive the event
	
	var $name;		//the name of the event
	var $target;	//the target box
	var $args;		//array that keeps the arguments of the event
	
	function Event($event_str)
	//Constructs an event from an event string.
	{
		global $sourcePage, $targetPage;
		
		$this->sourcePage = (isset($sourcePage) ? $sourcePage : "undefined");
		$this->targetPage = (isset($targetPage) ? $targetPage : "undefined");
		
		$this->load($event_str);
	}
	
	function load($event_str)
	//An event string has this format:
	//target.name(arg1=val1;arg2=val2;arg3=val3)
	{
		//extract event name and event args from $event_str
		$args_pos = strpos($event_str, "(");
		if ($args_pos)
		{
			$event_name = substr($event_str, 0, $args_pos);
			$event_args = substr($event_str, $args_pos);
		}
		else
		{
			$event_name = $event_str;
			$event_args = "";
		}
		
		//extract event target and event name from $event_name
		$dot_pos = strpos($event_name, ".");
		if ($dot_pos)
		{
			$this->target = substr($event_name, 0, $dot_pos);
			$this->name = substr($event_name, $dot_pos+1);
		}
		else
		{
			$this->target = "undefined";
			$this->name = $event_name;
		}
		
		//put the arguments in $this->args
		$this->args = array();
		if ($event_args<>"")
		{
			//strip off parantheses
			$event_args = substr($event_args, 1, strlen($event_args)-2);
			
			$args = explode(";", $event_args);
			while (list($i,$arg) = each($args))
			{
				if ($arg=="")	continue;
				list($arg_name,$arg_value) = split("=", $arg, 2);
				$this->args[$arg_name] = $arg_value;
			}
		}
	}
	
	function getArgs()
	//returns an array of all the arguments of the event
	//only their values
	{
		while ( list($arg_name,$arg_value) = each($this->args) )
		{
			$arg_values[] = $arg_value;
		}
		return $arg_values;
	}
	
	function to_text()	
	//prints out the event		(for debug)
	{
		print "Name: $this->name\n";
		print "Target: $this->target\n";
		print "Args: ";
		while (list($arg_name,$arg_value) = each($this->args))
		{
			print "$arg_name=$arg_value;";
		}
		print "\n\n";
	}
	
}	// End class Event

}	// End if defined

?>