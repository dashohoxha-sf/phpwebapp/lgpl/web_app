
function on_Add()
{
	alert("Add clicked");

	//do input validation and other processing here
	var form = document.addVarForm;
	var var_name = form.var_name.value;
	var var_value = form.var_value.value;
	if (var_name!="") session.AddVar(var_name, var_value);

	//now make the request for the next page
	GoTo("test.Session.php?event=nothing&test1=value 1&test2=value 2&arr[]=val1&arr[]=val2");
}

function on_Back()
{
	alert("Back clicked");
	history.go(-1);
}

function on_GoTo(page)
{
	alert("GoTo clicked");
	//do some client side processing here

	//make a request to the server
	GoTo(page);
}
