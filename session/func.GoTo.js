
//this variable is made true when the programer makes 'true'
//the DEBUG_GOTO constant in 'const.Settings.php'
var debug_GoTo = false;

function GoTo(page)
//submits the form of the page to the 'page' specified as parameter
//by taking care to serialize and transmit the session vars as well
{
	if (debug_GoTo==true)	alert("GoTo('"+page+"')");
	
	var form = document.WebAppForm;
	
	//the 'page' parameter is something like this:
	//"home.php?event=login&user=aaa&passw=bbb"
	idx	= page.indexOf("?");
	if (idx==-1)
	{
		target_page = page;
		php_vars = "";
	}
	else
	{
		target_page	= page.slice(0,idx);
		php_vars	= page.slice(idx+1);
	}
	
	//fill the form inputs
	var sourcePage	= form.sourcePage;
	var targetPage	= form.targetPage;
	var sessionVars	= form.sessionVars;
	var phpVars		= form.phpVars;
	
	if (target_page=="" || target_page=="thisPage")
	{
		targetPage.value = sourcePage.value;
	}
	else
	{
		targetPage.value = target_page;
	}
	sessionVars.value = session.toStr();
	phpVars.value = php_vars;
	
	//now submit the form
	form.submit();
}
